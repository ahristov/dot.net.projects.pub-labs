﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UsefulTips.Helpers;

namespace UsefulTips.Perf
{
	public class TimeItUsage : UsageBase, IUsage
	{
		public void Run()
		{
			using (var timer = new UsefulTips.Perf.TimeIt("Console.WriteLine", typeof(Console), 100))
			{
				for (int i = 0; i < 100; i++)
				{
					Console.WriteLine(i);
				}
			}

			Console.WriteLine("Press <Enter> to continue.");
			Console.ReadLine();
		}
	}
}
