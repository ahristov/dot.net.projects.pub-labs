﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace UsefulTips.Perf
{
	/// <summary>
	/// Use to measure time for running a code.
	/// </summary>
	/// <remarks>
	/// see: http://www.luisrocha.net/2011/12/data-access-performance-comparison-in.html
	/// <![CDATA[
	/// 
	///   try { System.IO.File.Delete("timeit.csv"); }
	///   catch { }
	///   System.Diagnostics.DelimitedListTraceListener csvListener = new System.Diagnostics.DelimitedListTraceListener("timeit.csv");
	///   System.Diagnostics.ConsoleTraceListener consoleListener = new System.Diagnostics.ConsoleTraceListener();
	///   
	///   System.Diagnostics.Trace.Listeners.Clear();
	///   System.Diagnostics.Trace.Listeners.Add(csvListener);
	///   System.Diagnostics.Trace.Listeners.Add(consoleListener);
	///   System.Diagnostics.Trace.AutoFlush = true;
	///   
	///   using (var timer = new UsefulTips.Perf.TimeIt("Console.WriteLine", typeof(Console), 100))
	///   {
	///       for (int i = 0; i < 100; i++)
	///       {
	///           Console.WriteLine(i);
	///       }
	///   }
	///   Console.WriteLine("Press <Enter> to continue");
	///   Console.ReadLine();
	///   
	/// ]]>
	/// </remarks>
	public class TimeIt : IDisposable
	{
		readonly Stopwatch _stopwatch = new Stopwatch();
		readonly String _testName;
		readonly Type _type;
		readonly Int32 _countRuns;

		public TimeIt(string testName, Type type, Int32 countRuns)
		{
			_testName = testName;
			_type = type;
			_countRuns = countRuns;
			_stopwatch.Start();
		}

		public void Dispose()
		{
			_stopwatch.Stop();
			Trace.WriteLine(string.Format("TimeIt,{0},{1},{2},{3},{4}", 
				_testName, 
				_type.Name, 
				"-", // method
				_countRuns, 
				_stopwatch.ElapsedMilliseconds));
		}

	}
}
