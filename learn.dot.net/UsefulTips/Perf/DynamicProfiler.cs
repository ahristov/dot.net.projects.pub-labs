﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace UsefulTips.Perf
{
	/// <summary>
	/// Implements the Adapter pattern.
	/// </summary>
	/// <remarks>
	/// see: http://osmirnov.net/posts/dynamicobject-aop-for-the-poor/
	/// </remarks>
	public class DynamicProfiler : DynamicWrapper, IDisposable
	{
		private string _testName;
		private int _iterationCount;
		private Stopwatch _objectStopwatch;

		public DynamicProfiler(string testName, object source, int iterationCount)
			: base(source)
		{
			_testName = testName;
			_iterationCount = iterationCount;
			
			_objectStopwatch = new Stopwatch();
			_objectStopwatch.Start();
		}

		protected override object MethodCall(MethodInfo methodInfo, Func<object, object[], object> func, object src, object[] args)
		{
			object result = null;

			Stopwatch methodWatch = new Stopwatch();
			methodWatch.Start();

			for (int i = 0; i < _iterationCount; i++)
			{
				result = base.MethodCall(methodInfo, func, src, args);	

			}

			methodWatch.Stop();

			Trace.WriteLine(string.Format("DynamicProfiler,{0},{1},{2},{3},{4}", 
				_testName,
				_source.GetType().Name, 
				methodInfo.Name,
				_iterationCount,
				methodWatch.ElapsedMilliseconds));

			return result;
		}


		public void Dispose()
		{
			_objectStopwatch.Stop();

			Trace.WriteLine(string.Format("DynamicProfiler,{0},{1},{2},{3},{4}",
				_testName,
				_source.GetType().Name,
				"-",
				_iterationCount,
				_objectStopwatch.ElapsedMilliseconds));

		}
	}
}
