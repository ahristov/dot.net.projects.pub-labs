﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;
using System.Reflection;

namespace UsefulTips.Perf
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// see: http://osmirnov.net/posts/dynamicobject-aop-for-the-poor/
	/// </remarks>
	public class DynamicWrapper : DynamicObject
	{
		protected readonly object _source;

		public DynamicWrapper(object source)
		{
			_source = source;
		}

		public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
		{
			var methodInfo = _source.GetType().GetMethod(binder.Name);
			if (methodInfo != null)
			{
				Func<object, object[], object> func = (s, a) => methodInfo.Invoke(s, a);

				result = MethodCall(methodInfo, func, _source, args);

				return true;
			}

			result = null;

			return false;
		}

		protected virtual object MethodCall(MethodInfo methodInfo, Func<object, object[], object> func, object src, object[] args)
		{
			return func(src, args);
		}

	}
}
