﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UsefulTips.Helpers;
using UsefulTips.Perf;
using UsefulTips.Collections;


namespace UsefulTips
{
	class Program
	{
		static void Main(string[] args)
		{
			IUsage usage;

			usage = new SortedCollections();
			usage.Run();

			Console.WriteLine("Press <Enter> to exit.");
			Console.ReadLine();
		}
	}
}
