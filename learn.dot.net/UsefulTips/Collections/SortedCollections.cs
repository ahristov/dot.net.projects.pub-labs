﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UsefulTips.Helpers;
using UsefulTips.Perf;

namespace UsefulTips.Collections
{
	public class SortedCollections : UsageBase, IUsage
	{
		public void Run()
		{
			SortedList<int, int> sl = new SortedList<int, int>(); // foreach sorted, use mainly when insertion ordered by key
			SortedDictionary<int, int> sd = new SortedDictionary<int, int>(); // foreach sorted, use when insertion no ordered by key
			Dictionary<int, int> d = new Dictionary<int, int>(); // faster, use always inless foreach access needed to be sorted

			int countRuns = 100000;



			using (var timer = new TimeIt("SortedList.Add", typeof(SortedList<int, int>), countRuns))
			{
				for (int i = 0; i < countRuns; i++)
				{
					sl.Add(i, i);
				}
			}

			using (var timer = new TimeIt("SortedDictionary.Add", typeof(SortedDictionary<int,int>), countRuns))
			{
				for (int i = 0; i < countRuns; i++)
				{
					sd.Add(i, i);
				}
			}

			using (var timer = new TimeIt("Dictionary.Add", typeof(Dictionary<int, int>), countRuns))
			{
				for (int i = 0; i < countRuns; i++)
				{
					d.Add(i, i);
				}
			}



			using (var timer = new TimeIt("SortedList.Get", typeof(SortedList<int, int>), countRuns))
			{
				for (int i = 0; i < countRuns; i++)
				{
					int value = sl[i];
				}
			}

			using (var timer = new TimeIt("SortedDictionary.Get", typeof(SortedDictionary<int, int>), countRuns))
			{
				for (int i = 0; i < countRuns; i++)
				{
					int value = sd[i];
				}
			}

			using (var timer = new TimeIt("Dictionary.Get", typeof(Dictionary<int, int>), countRuns))
			{
				for (int i = 0; i < countRuns; i++)
				{
					int value = d[i];
				}
			}

			
			
			// > 10 times slower that timeIt

			using (dynamic profiler = new DynamicProfiler("SortedList.Get", sl, countRuns))
			{
				for (int i = 0; i < countRuns; i++)
				{
					int value = sl[i];
				}
			}

			using (dynamic profiler = new DynamicProfiler("SortedDictionary.Get", sd, countRuns))
			{
				for (int i = 0; i < countRuns; i++)
				{
					int value = sd[i];
				}
			}

			using (dynamic profiler = new DynamicProfiler("SortedCollections.Get", d, countRuns))
			{
				for (int i = 0; i < countRuns; i++)
				{
					int value = d[i];
				}
			}



			Console.WriteLine("Press <Enter> to continue.");
			Console.ReadLine();


		}
	}
}
