﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UsefulTips.Perf;

namespace UsefulTips.Helpers
{
	public abstract class UsageBase
	{
		private static bool _initialized;
		private static object _locker = new object();

		static UsageBase()
		{
			lock(_locker)
			{
				if (! _initialized)
				{
					try { System.IO.File.Delete("timeit.csv"); }
					catch { }
					System.Diagnostics.DelimitedListTraceListener csvListener = new System.Diagnostics.DelimitedListTraceListener("timeit.csv");
					System.Diagnostics.ConsoleTraceListener consoleListener = new System.Diagnostics.ConsoleTraceListener();

					System.Diagnostics.Trace.Listeners.Clear();
					System.Diagnostics.Trace.Listeners.Add(csvListener);
					System.Diagnostics.Trace.Listeners.Add(consoleListener);
					System.Diagnostics.Trace.AutoFlush = true;

					_initialized = true;
				}
			}
		}

	}
}
