﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UsefulTips.Helpers
{
	public interface IUsage
	{
		void Run();
	}
}
