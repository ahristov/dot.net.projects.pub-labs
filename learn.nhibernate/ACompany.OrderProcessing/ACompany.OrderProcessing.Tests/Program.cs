﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;
using System.Collections.ObjectModel;
using ACompany.OrderProcessing.NHibernateRepository;


namespace ACompany.OrderProcessing.Tests
{
    class Program
    {
        static void Main(string[] args)
        {
			SessionFactory factory = new SessionFactory();

			ICustomerRepository repo = new CustomerRepository(factory);

			var foo = repo.FindAll();

            Address address = new Address("Street1", "City1", "Country1", "postCode1");

            String street = address.Street;
            street = "Street2";

            Console.WriteLine(address.Street);

            Console.WriteLine("Press <Enter> to finish");
            Console.ReadLine();

        }
    }
}
