﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Infrastructure;

namespace ACompany.OrderProcessing.Model
{
    public class Product : EntityBase<Product>, IAggregateRoot
    {
        public Product() { }

        public String Name { get; set; }
        public Decimal Price { get; set; }
        public Int32 Stock { get; set; }


        protected override void Validate()
        {
            if (Stock < 0)
            {
                BrokenRules.Add("Stock cannot be negative.");
            }

            if (Price < 0)
            {
                BrokenRules.Add("Price cannot be negative.");
            }

            if (String.IsNullOrEmpty(Name))
            {
                BrokenRules.Add("Name is required value.");
            }
            else
            {
                if (Name.Length > 50)
                {
                    BrokenRules.Add("Name should be 50 chars or less.");
                }
            }
        }
    }
}
