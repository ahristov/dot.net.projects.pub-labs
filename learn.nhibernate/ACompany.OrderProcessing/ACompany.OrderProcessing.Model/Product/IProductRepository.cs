﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Infrastructure;

namespace ACompany.OrderProcessing.Model
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
