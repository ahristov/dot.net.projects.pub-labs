﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Model
{
    public interface IProductService
    {
        IList<Product> FindAll();
        Product FindBy(Guid id);
        void Save(Product product);
        void Remove(Product product);
    }
}
