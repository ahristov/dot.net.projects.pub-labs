﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Infrastructure;

namespace ACompany.OrderProcessing.Model
{
    public class Address : ValueObjectBase
    {
        private String _street;
        private String _city;
        private String _country;
        private String _postCode;

		public Address() { }

        public Address(String street, String city, String country, String postCode)
        {
            _street = street;
            _city = city;
            _country = country;
            _postCode = postCode;
        }


        public String Street
        {
            get { return _street; }
        }

        public String City
        {
            get { return _city; }
        }

        public String Country
        {
            get { return _country; }
        }

        public String PostCode
        {
            get { return _postCode; }
        }



        protected override void Validate()
        {
            StringBuilder brokenRules = new StringBuilder();

            if (String.IsNullOrEmpty(Street))
            {
                brokenRules.AppendLine("Street is a required value.");
            }
            else
            {
                if (Street.Length > 50)
                {
                    brokenRules.AppendLine("Street should be 50 chars or less.");
                }
            }

            if (String.IsNullOrEmpty(City))
            {
                brokenRules.AppendLine("City is a required value.");
            }
            else
            {
                if (City.Length > 50)
                {
                    brokenRules.AppendLine("City should be 50 chars or less.");
                }
            }


            if (String.IsNullOrEmpty(Country))
            {
                brokenRules.AppendLine("Country is a required value.");
            }
            else
            {
                if (Country.Length > 50)
                {
                    brokenRules.AppendLine("Country should be 50 chars or less.");
                }
            }


            if (String.IsNullOrEmpty(PostCode))
            {
                brokenRules.AppendLine("PostCode is a required value.");
            }
            else
            {
                if (PostCode.Length > 50)
                {
                    brokenRules.AppendLine("PostCode should be 50 chars or less.");
                }
            }



            if (! String.IsNullOrEmpty(brokenRules.ToString()))
            {
                throw new ArgumentException(
                    String.Format(
                        "Broken rules on Address, pleace correct the following errors:{0}{1}",
                        Environment.NewLine,
                        brokenRules.ToString()
                    )
                );
            }


        }
    }
}
