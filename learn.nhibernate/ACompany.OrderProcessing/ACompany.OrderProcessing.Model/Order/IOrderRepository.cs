﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Infrastructure;

namespace ACompany.OrderProcessing.Model
{
    public interface IOrderRepository : IRepository<Order>
    {
        IList<Order> FindAllOrdersContainingProductBy(Guid productId);
    }
}
