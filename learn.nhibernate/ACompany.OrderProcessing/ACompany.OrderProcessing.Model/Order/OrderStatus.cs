﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Model
{
    public enum OrderStatus
    {
        Placed,
        Shipped,
        Cancelled
    }
}
