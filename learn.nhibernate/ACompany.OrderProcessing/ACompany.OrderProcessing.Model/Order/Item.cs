﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Infrastructure;

namespace ACompany.OrderProcessing.Model
{
    public class Item : EntityBase<Item>
    {
        public Item() { }

        public Order Order { get; set; }
        public Product Product { get; set; }
        public Int32 Qty { get; set; }
        public Decimal Price { get; set; }


        protected override void Validate()
        {
            if (Qty <= 0)
            {
                BrokenRules.Add("An item qty must be greater than zero.");
            }
            
            if (Order == null)
            {
                BrokenRules.Add("An item must have parent order.");
            }

            if (Product == null)
            {
                BrokenRules.Add("An item must contain a product.");
            }

            if (Price < 0)
            {
                BrokenRules.Add("The price of the item can not be negative.");
            }
        }
    }
}
