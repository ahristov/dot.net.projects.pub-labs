﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Model
{
	public interface IOrderService
	{
		Order FindBy(Guid id);
		void Place(Order order);
		IList<Order> GetAllOrdersBy(OrderStatus status);
		void Process(Order order);
		void Save(Order order);
	}
}
