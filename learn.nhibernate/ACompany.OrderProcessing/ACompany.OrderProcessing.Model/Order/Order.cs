﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using ACompany.OrderProcessing.Infrastructure;

namespace ACompany.OrderProcessing.Model
{
    public class Order : EntityBase<Order>, IAggregateRoot
    {
        private DateTime _orderDate;
        private Customer _customer;
        private Address _despatchAddress;
        private OrderStatus _status;
        private List<Item> _items;
        private Int32 _version;

        public Order()
        {
            _status = OrderStatus.Placed;
            _orderDate = DateTime.Now;
            _items = new List<Item>();
            _version = 0;
        }


        public DateTime OrderDate
        {
            get { return _orderDate; }
            set { _orderDate = value; }
        }

        public Customer Customer 
        {
            get { return _customer; }
            set { _customer = value; } 
        }

        public Address DespatchAddress 
        {
            get { return _despatchAddress; }
            set { _despatchAddress = value; } 
        }

        public OrderStatus Status  
        {
            get { return _status; } 
        }

        public ReadOnlyCollection<Item> Items 
        {
            get
            {
                return new ReadOnlyCollection<Item>(_items);
            }
        }




        #region "Version for concurency checking"
        
        public Int32 Version
        {
            get { return _version; }
        }

        #endregion "Version for concurency checking"





        #region "Methods"

        public void Remove(Item item)
        {
            _items.Remove(item);
        }

        public void Add(Product product)
        {
            if (OrderContains(product))
            {
                Item newItem = new Item();
                newItem.Order = this;
                newItem.Product = product;
                newItem.Qty = 1;
                newItem.Price = product.Price;
                _items.Add(newItem);
            }
            else
            {
                IncreaseQtyOfItemWithMatching(product);
            }
        }


        private void IncreaseQtyOfItemWithMatching(Product product)
        {
            foreach (Item item in _items)
            {
                if (item.Product.Equals(product))
                {
                    item.Qty += 1;
                    break;
                }
            }
        }


        private Boolean OrderContains(Product product)
        {
            Boolean found = false;

            foreach (Item item in _items)
            {
                if (item.Product.Id == product.Id)
                {
                    found = true;
                    break;
                }
            }

            return found;

        }


        public Boolean CanCancel
        {
			get
			{
				return _status == OrderStatus.Placed;
			}
        }

        public Boolean CanProcess
        {
			get
			{
				if (_status == OrderStatus.Placed)
				{
					return HaveEnoughStockToProcess();
				}
				else
				{
					return false;
				}
			}
        }

        public void Cancel()
        {
            if (this.CanCancel)
            {
                _status = OrderStatus.Cancelled;
            }
            else
            {
                throw new InvalidOperationException("This order can not be cancelled");
            }
        }

        public void Process()
        {
            if (this.CanProcess)
            {
                _status = OrderStatus.Shipped;
            }
            else
            {
                throw new InvalidOperationException("This order can not be processed");
            }
        }


        public Boolean HaveEnoughStockToProcess()
        {
            Boolean enoughStock = true;

            foreach (Item item in _items)
            {
                if (item.Qty > item.Product.Stock)
                {
                    enoughStock = false;
                    break;
                }
            }

            return enoughStock;
        }


        #endregion "Methods"


        protected override void Validate()
        {
            if (Items == null)
            {
                BrokenRules.Add("An order must have at least one order item.");
            }
            else
            {
                if (Items.Count == 0)
                {
                    BrokenRules.Add("An order must have at least one order item.");
                }
                else
                {
                    foreach (Item item in Items)
                    {
                        foreach (String brokenRule in item.GetBrokenRules())
                        {
                            BrokenRules.Add(brokenRule);
                        }
                    }
                }
            }

            if (Customer == null)
            {
                BrokenRules.Add("An order must have a customer.");
            }

            if (DespatchAddress == null)
            {
                BrokenRules.Add("An order must have a despatch address.");
            }

            if (!Enum.IsDefined(typeof(OrderStatus), Status))
            {
                BrokenRules.Add("An order must have a valid status.");
            }
        }
    }
}
