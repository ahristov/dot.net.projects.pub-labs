﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Infrastructure;

namespace ACompany.OrderProcessing.Model
{
    public class Customer : EntityBase<Customer>, IAggregateRoot
    {
        private String _name;
        private String _email;
        private Address _address;
        private IList<Order> _orders;


        public Customer()
        {
            _orders = new List<Order>();
        }


        public String Name 
        {
            get { return _name; }
            set { _name = value; } 
        }

        public String Email 
        {
            get { return _email; }
            set { _email = value; } 
        }

        public Address Address 
        {
            get { return _address; }
            set { _address = value; } 
        }

        public IList<Order> Orders 
        {
            get { return _orders; }
        }




        protected override void Validate()
        {
            if (Address == null)
            {
                BrokenRules.Add("A Customer must have an address.");
            }

            if (String.IsNullOrEmpty(Email))
            {
                BrokenRules.Add("Email is required value.");
            }
            else
            {
                if (Email.Length > 50)
                {
                    BrokenRules.Add("Email should be 50 chars or less.");
                }
            }

            if (String.IsNullOrEmpty(Name))
            {
                BrokenRules.Add("Name is required value.");
            }
            else
            {
                if (Name.Length > 50)
                {
                    BrokenRules.Add("Name should be 50 chars or less.");
                }
            }

        }
    }
}
