﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Model
{
	public interface ICustomerService
	{
		IList<Customer> FindAll();
		Customer FindBy(Guid id);
		void Save(Customer customer);
		void Remove(Customer customer);
	}
}
