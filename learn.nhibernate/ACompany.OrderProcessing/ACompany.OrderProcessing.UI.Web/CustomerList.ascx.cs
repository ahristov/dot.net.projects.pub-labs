﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ACompany.OrderProcessing.Presentation;
using ACompany.OrderProcessing.Infrastructure;
using ACompany.OrderProcessing.Model;
using ACompany.OrderProcessing.Services;

namespace ACompany.OrderProcessing.UI.Web
{
	public partial class CustomerList : System.Web.UI.UserControl, ICustomerListView
	{
		private ICustomerListPresenter _presenter;
		private ICustomerService _customerService;
		private ICustomerRepository _customerRepository;
		private NHibernateRepository.SessionFactory _sessionFactory;

		protected void Page_Init(object sender, EventArgs e)
		{
			_sessionFactory = new NHibernateRepository.SessionFactory();
			_customerRepository = new NHibernateRepository.CustomerRepository(_sessionFactory);
			_customerService = new CustomerService(_customerRepository);
			_presenter = new CustomerListPresenter(this, _customerService);

			this.CustomersGridView.RowCommand += new GridViewCommandEventHandler(CustomersGridView_RowCommand);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			_presenter.Display();
		}

		public IList<Customer> Customers
		{
			set 
			{
				this.CustomersGridView.DataSource = value;
				this.CustomersGridView.DataBind();
			}
		}

		public void SetErrorMessage(string message)
		{
			message = "" + message;

			if (String.IsNullOrEmpty(message))
			{
				ErrorMessagePanel.Visible = false;
			}
			else
			{
				ErrorMessagePanel.Visible = true;
			}

			ErrorMessageLiteral.Text = message.Replace(Environment.NewLine, "<br/>");
		}

		public void SetInfoMessage(string message)
		{
			message = "" + message;

			if (String.IsNullOrEmpty(message))
			{
				InfoMessagePanel.Visible = false;
			}
			else
			{
				InfoMessagePanel.Visible = true;
			}

			InfoMessageLiteral.Text = message.Replace(Environment.NewLine, "<br/>");
			
		}

		public void SetViewTitle(string title)
		{
			title = "" + title;

			TitleLiteral.Text = title;
		}



		void CustomersGridView_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.ToLower() == "deletecustomer")
			{
				_presenter.RemoveCustomerBy(new Guid(e.CommandArgument.ToString()));
			}
		}

	}
}