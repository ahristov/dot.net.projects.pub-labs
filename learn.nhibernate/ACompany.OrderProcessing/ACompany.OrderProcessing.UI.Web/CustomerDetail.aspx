﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerDetail.aspx.cs" Inherits="ACompany.OrderProcessing.UI.Web.CustomerDetail1" %>

<%@ Register Src="CustomerDetail.ascx" TagName="CustomerDetail" TagPrefix="UCtl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<UCtl:CustomerDetail ID="CustomerDetailUserControl" runat="server" />
    </div>
    </form>
</body>
</html>
