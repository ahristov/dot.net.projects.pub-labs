﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerDetail.ascx.cs" Inherits="ACompany.OrderProcessing.UI.Web.CustomerDetail" %>


<h3><asp:Literal ID="TitleLiteral" runat="server"></asp:Literal></h3>

<asp:Panel ID="ErrorMessagePanel" runat="server">
	<table  style="border: thin solid #FF0000">
		<tr>
			<td>
				<font color="#FF0000">
					<asp:Literal ID="ErrorMessageLiteral" runat="server"></asp:Literal>
				</font>
			</td>
		</tr>
	</table>
</asp:Panel>

<asp:Panel ID="InfoMessagePanel" runat="server">
	<table style="border: thin solid ##00FF00">
		<tr>
			<td>
				<font color="#00FF00">
					<asp:Literal ID="InfoMessageLiteral" runat="server"></asp:Literal>
				</font>
			</td>
		</tr>
	</table>
</asp:Panel>

<table>
	<tr>
		<td>Name</td>
		<td><asp:TextBox ID="NameTextBox" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>Email</td>
		<td><asp:TextBox ID="EmailTextBox" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>Street</td>
		<td><asp:TextBox ID="StreetTextBox" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>City</td>
		<td><asp:TextBox ID="CityTextBox" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>Country</td>
		<td><asp:TextBox ID="CountryTextBox" runat="server"></asp:TextBox></td>
	</tr>
	<tr>
		<td>PostCode</td>
		<td><asp:TextBox ID="PostCodeTextBox" runat="server"></asp:TextBox></td>
	</tr>
</table>

<asp:Button ID="SaveCustomerButton" runat="server" Text="Save" />
<asp:Button ID="CancelEditCustomer" runat="server" Text="Back to Customer List" />
