﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ACompany.OrderProcessing.Presentation;
using ACompany.OrderProcessing.Infrastructure;
using ACompany.OrderProcessing.Model;
using ACompany.OrderProcessing.Services;

namespace ACompany.OrderProcessing.UI.Web
{
	public partial class CustomerDetail : System.Web.UI.UserControl, ICustomerView
	{

		private ICustomerPresenter _presenter;
		private ICustomerService _customerService;
		private ICustomerRepository _customerRepository;
		private NHibernateRepository.SessionFactory _sessionFactory;

		protected void Page_Init(object sender, EventArgs e)
		{
			_sessionFactory = new NHibernateRepository.SessionFactory();
			_customerRepository = new NHibernateRepository.CustomerRepository(_sessionFactory);
			_customerService = new CustomerService(_customerRepository);
			_presenter = new CustomerPresenter(this, _customerService);

		}


		protected void Page_Load(object sender, EventArgs e)
		{
			if (! IsPostBack)
			{
				_presenter.Display();
			}
		}

		public Guid CustomerId
		{
			get
			{
				Guid SelectedCustomerId;

				if (String.IsNullOrEmpty(Request.QueryString["CustomerId"]))
				{
					SelectedCustomerId = Guid.Empty;
				}
				else
				{
					SelectedCustomerId = new Guid(Request.QueryString["CustomerId"]);
				}

				return SelectedCustomerId;
			}
		}

		public string Name
		{
			get { return NameTextBox.Text; }
			set { NameTextBox.Text = value; }
		}

		public string Email
		{
			get { return EmailTextBox.Text; }
			set { EmailTextBox.Text = value; }
		}

		public string Street
		{
			get { return StreetTextBox.Text; }
			set { StreetTextBox.Text = value; }
		}

		public string City
		{
			get { return CityTextBox.Text; }
			set { CityTextBox.Text = value; }
		}

		public string Country
		{
			get { return CountryTextBox.Text; }
			set { CountryTextBox.Text = value; }
		}

		public string PostCode
		{
			get { return PostCodeTextBox.Text; }
			set { PostCodeTextBox.Text = value; }
		}


		public string SaveButtonText
		{
			set { SaveCustomerButton.Text = value; }
		}

		public void SetErrorMessage(string message)
		{
			message = "" + message;

			if (String.IsNullOrEmpty(message))
			{
				ErrorMessagePanel.Visible = false;
			}
			else
			{
				ErrorMessagePanel.Visible = true;
			}

			ErrorMessageLiteral.Text = message.Replace(Environment.NewLine, "<br/>");
		}

		public void SetInfoMessage(string message)
		{
			message = "" + message;

			if (String.IsNullOrEmpty(message))
			{
				InfoMessagePanel.Visible = false;
			}
			else
			{
				InfoMessagePanel.Visible = true;
			}

			InfoMessageLiteral.Text = message.Replace(Environment.NewLine, "<br/>");

		}

		public void SetViewTitle(string title)
		{
			title = "" + title;

			TitleLiteral.Text = title;
		}
	}
}