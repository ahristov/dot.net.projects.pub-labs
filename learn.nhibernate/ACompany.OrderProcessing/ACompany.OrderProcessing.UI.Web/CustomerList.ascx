﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerList.ascx.cs" Inherits="ACompany.OrderProcessing.UI.Web.CustomerList" %>

<h3><asp:Literal ID="TitleLiteral" runat="server"></asp:Literal></h3>

<asp:Panel ID="ErrorMessagePanel" runat="server">
	<table  style="border: thin solid #FF0000">
		<tr>
			<td>
				<font color="#FF0000">
					<asp:Literal ID="ErrorMessageLiteral" runat="server"></asp:Literal>
				</font>
			</td>
		</tr>
	</table>
</asp:Panel>

<asp:Panel ID="InfoMessagePanel" runat="server">
	<table style="border: thin solid ##00FF00">
		<tr>
			<td>
				<font color="#00FF00">
					<asp:Literal ID="InfoMessageLiteral" runat="server"></asp:Literal>
				</font>
			</td>
		</tr>
	</table>
</asp:Panel>


<asp:GridView ID="CustomersGridView" 
			DataKeyNames="Id"
			runat="server"
			AutoGenerateColumns="false"
			CellPadding="4"
			ForeColor="#333333"
			GridLines="None"
			Width="573px">

	<RowStyle BackColor="#EFF3FB" />
	<Columns>
		<asp:BoundField DataField="Name" HeaderText="Name" />
		<asp:BoundField DataField="Email" HeaderText="Email" />
		<asp:HyperLinkField DataNavigateUrlFormatString="CustomerDetail.aspx?CustomerId={0}"
							Text="Edit" />
		<asp:TemplateField ShowHeader="false">
			<ItemTemplate>
				<asp:LinkButton	ID="DeleteLinkButton" 
								CausesValidation="false"
								CommandArgument='<%#Eval("Id") %>'
								CommandName="DeleteCustomer"
								Text="Delete"
								OnClientClick="return confirm('Are you sure you want to delete this customer?');" 
								runat="server" />
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>

	<FooterStyle BackColor="#507CD1" Font-Bold="true" ForeColor="White" />
	<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
	<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="true" ForeColor="#333333" />
	<HeaderStyle BackColor="#507CD1" Font-Bold="true" ForeColor="White" />
	<EditRowStyle BackColor="#2461BF" />
	<AlternatingRowStyle BackColor="White" />
				
</asp:GridView>

<br />

<asp:Button ID="NewCustomerButton" runat="server" Text="Add Customer" />



