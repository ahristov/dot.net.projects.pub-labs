﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using NHibernate.Tool.hbm2ddl;

namespace ACompany.OrderProcessing.NHibernateRepository
{
    public class SessionFactory
    {
        private static NHibernate.ISessionFactory _sessionFactory;

        private static void Init()
        {
            NHibernate.Cfg.Configuration config;
            config = new NHibernate.Cfg.Configuration();
            
			// As long as we have an empty database with the name 
			// as the one from our connection string NHibernate will generate
			// the schema for us.
			//
			// It will truncate the database and rebuild it?
			//
			//SchemaExport schemaExport = new SchemaExport(config);
			//schemaExport.Execute(false, true, false);
			
			config.Configure();

            // Ensure mapping files are embeded in the assembly
            // config.AddAssembly("NHibernate.QuickDemo.NHibernateDAL");
            config.AddAssembly(Assembly.GetExecutingAssembly());
            _sessionFactory = config.BuildSessionFactory();
        }

        internal static NHibernate.ISessionFactory GetSessionFactory()
        {
            if (_sessionFactory == null)
            {
                Init();
            }

            return _sessionFactory;
        }

		public NHibernate.ISession GetCurrentSession()
		{
			if (!NHibernate.Context.CurrentSessionContext.HasBind(GetSessionFactory()))
			{
				return GetSessionFactory().OpenSession();
			}
			else
			{
				return GetSessionFactory().GetCurrentSession();
			}
		}

        public NHibernate.ISession GetNewSession()
        {
            return GetSessionFactory().OpenSession();
        }

    }
}
