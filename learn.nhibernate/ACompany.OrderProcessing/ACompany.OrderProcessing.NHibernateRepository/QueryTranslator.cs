﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using ACompany.OrderProcessing.Infrastructure;
using ACompany.OrderProcessing.Infrastructure.Query;
using NHibernate.Criterion;

namespace ACompany.OrderProcessing.NHibernateRepository
{
	public class QueryTranslator
	{
		public static ICriteria GetCriteriaFrom(
			ICriteria criteria,
			Query query)
		{
			foreach (Criteria c in query.Criteria)
			{
				switch (c.CriteriaOperator)
				{
					case CriteriaOperator.Equals:
						criteria.Add(Expression.Eq(c.PropertyName, c.Value));
						break;
					case CriteriaOperator.Like:
						criteria.Add(Expression.Like(c.PropertyName, c.Value));
						break;
					default:
						break;
				}
			}

			return criteria;
		}
	}
}
