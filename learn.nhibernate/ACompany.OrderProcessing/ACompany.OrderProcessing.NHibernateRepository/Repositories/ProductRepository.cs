﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;

namespace ACompany.OrderProcessing.NHibernateRepository
{
    public class ProductRepository 
        : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(SessionFactory sessionFactory)
            : base(sessionFactory)
        { }
    }
}
