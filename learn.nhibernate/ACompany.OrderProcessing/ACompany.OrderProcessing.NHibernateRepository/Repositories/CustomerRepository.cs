﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;

namespace ACompany.OrderProcessing.NHibernateRepository
{
    public class CustomerRepository 
        : RepositoryBase<Customer>, ICustomerRepository
    {

        public CustomerRepository(SessionFactory sessionFactory)
            : base(sessionFactory)
        { }
    }
}
