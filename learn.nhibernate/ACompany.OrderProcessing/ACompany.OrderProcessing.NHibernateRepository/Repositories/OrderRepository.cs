﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;
using NHibernate;

namespace ACompany.OrderProcessing.NHibernateRepository
{
    public class OrderRepository 
        : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(SessionFactory sessionFactory)
            : base(sessionFactory)
        { }

        public IList<Order> FindAllOrdersContainingProductBy(Guid productId)
        {
			return _sessionFactory.GetCurrentSession()
				.CreateCriteria<Order>()
				.CreateCriteria("Items")
				.Add(NHibernate.Criterion.Expression.Eq("Product.Id", productId))
				.List<Order>();

			//using (ISession session = _sessionFactory.GetNewSession())
			//{
			//    return session.CreateCriteria<Order>()
			//        .CreateCriteria("Items")
			//        .Add(NHibernate.Criterion.Expression.Eq("Product.Id", productId))
			//        .List<Order>();
			//}

        }
    }
}
