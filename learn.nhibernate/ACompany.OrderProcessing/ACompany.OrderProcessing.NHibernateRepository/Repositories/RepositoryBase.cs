﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Infrastructure;
using ACompany.OrderProcessing.Infrastructure.Query;
using NHibernate;

namespace ACompany.OrderProcessing.NHibernateRepository
{
    public abstract class RepositoryBase<TAggregateRoot>
        : IRepository<TAggregateRoot>
        where TAggregateRoot : EntityBase<TAggregateRoot>, IAggregateRoot
    {
        protected SessionFactory _sessionFactory;
		private IUnitOfWork _unitOfWork;

        public RepositoryBase(SessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }


		public void Save(TAggregateRoot entity)
        {
			if (_unitOfWork == null)
			{
				using (ITransaction tx = _sessionFactory.GetCurrentSession().BeginTransaction())
				{
					_sessionFactory.GetCurrentSession().SaveOrUpdate(entity);
					try
					{
						tx.Commit();
					}
					catch (StaleObjectStateException nhEx)
					{
						// rollback the transaction
						tx.Rollback();

						// entity was changed while we were viewing it,
						// wrap NHibernate exception and send it back
						// up the calling stack
						throw new ConcurrencyException(
							String.Format("Please reload the {0} " +
								"as changes have been made to it since you have loaded it.",
								entity.Id.ToString()),
							nhEx);
					}
					catch (Exception ex)
					{
						// rollback the transaction
						tx.Rollback();

						// throw the exception back up to the stack
						throw ex;
					}
				}
			}
			else
			{
				_unitOfWork.Save(entity);
			}

			//using (ISession session = _sessionFactory.GetNewSession())
			//{
			//    using (ITransaction tx = session.BeginTransaction())
			//    {
			//        session.SaveOrUpdate(entity);
			//        tx.Commit();
			//    }
			//}
        }

        public void Remove(TAggregateRoot entity)
        {
			if (_unitOfWork == null)
			{
				using (ITransaction tx = _sessionFactory.GetCurrentSession().BeginTransaction())
				{
					_sessionFactory.GetCurrentSession().Delete(entity);
					try
					{
						tx.Commit();
					}
					catch (Exception ex)
					{
						// rollback the transaction
						tx.Rollback();

						// throw the exception back to the stack
						throw ex;
					}
				}
			}
			else
			{
				_unitOfWork.Delete(entity);
			}

			//using (ISession session = _sessionFactory.GetNewSession())
			//{
			//    using (ITransaction tx = session.BeginTransaction())
			//    {
			//        session.Delete(entity);
			//        tx.Commit();
			//    }
			//}
        }

        public TAggregateRoot FindBy(Guid Id)
        {
			return _sessionFactory.GetCurrentSession().Get<TAggregateRoot>(Id);

			//using (ISession session = _sessionFactory.GetNewSession())
			//{
			//    return session.Get<TAggregateRoot>(Id);
			//}
        }

		public IList<TAggregateRoot> FindBy(Query query)
		{
			ICriteria criteria = _sessionFactory.GetCurrentSession()
				.CreateCriteria<TAggregateRoot>();

			return QueryTranslator.GetCriteriaFrom(criteria, query)
				.List<TAggregateRoot>();
		}


        public IList<TAggregateRoot> FindAll()
        {
			return _sessionFactory.GetCurrentSession()
				.CreateCriteria<TAggregateRoot>()
				.List<TAggregateRoot>();

			//using (ISession session = _sessionFactory.GetNewSession())
			//{
			//    ICriteria qry = session.CreateCriteria<TAggregateRoot>(); //  (typeof(TAggregateRoot));

			//    return qry.List<TAggregateRoot>();
			//}
        }


		public void Inject(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}


	}
}
