﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ACompany.OrderProcessing.NHibernateRepository
{
	public class NHibernateHttpModule : IHttpModule
	{
		public void Init(HttpApplication context)
		{
			context.BeginRequest += new EventHandler(context_BeginRequest);
			context.EndRequest += new EventHandler(context_EndRequest);
		}

		
		void context_BeginRequest(object sender, EventArgs e)
		{
			NHibernate.Context.CurrentSessionContext.Bind(
				new SessionFactory().GetNewSession());
		}

	
		void context_EndRequest(object sender, EventArgs e)
		{
			NHibernate.ISession currentSession;

			currentSession = NHibernate.Context.CurrentSessionContext.Unbind(
				SessionFactory.GetSessionFactory());

			if (currentSession != null)
			{
				currentSession.Close();
				currentSession.Dispose();
				currentSession = null;
			}
		}

		
		public void Dispose()
		{
		}

	}
}
