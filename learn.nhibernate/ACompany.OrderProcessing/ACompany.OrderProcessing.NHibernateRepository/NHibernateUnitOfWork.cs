﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Infrastructure;

namespace ACompany.OrderProcessing.NHibernateRepository
{
	public class NHibernateUnitOfWork : IUnitOfWork
	{
		NHibernate.ISession _session;

		public NHibernateUnitOfWork()
		{
			_session = new SessionFactory().GetCurrentSession();
		}

		public void Save(IAggregateRoot entity)
		{
			_session.SaveOrUpdate(entity);
		}

		public void Delete(IAggregateRoot entity)
		{
			_session.Delete(entity);
		}

		public void Commit()
		{
			using (NHibernate.ITransaction tx = _session.BeginTransaction())
			{
				try
				{
					// flush the session and commit the transaction
					tx.Commit();
				}
				catch (NHibernate.StaleObjectStateException nhEx)
				{
					// rollback the transaction
					tx.Rollback();

					// entity was changed while we were viewing it,
					// wrap NHibernate exception and send it back
					// up the calling stack
					throw new ConcurrencyException(
						"Please reload the screen as changes " +
						"have been made to an entity since first viewwing.",
						nhEx);
				}
				catch (Exception ex)
				{
					// rollback the transaction
					tx.Rollback();

					// rethrow the exception
					throw ex;
				}
			}
		}
	}
}
