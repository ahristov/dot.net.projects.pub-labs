﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;

namespace ACompany.OrderProcessing.Services
{
    public class ProductService : IProductService
    {
        IProductRepository _productRepository;
        IOrderRepository _orderRepository;

        public ProductService(
            IProductRepository productRepository,
            IOrderRepository orderRepository
        )
        {
            _productRepository = productRepository;
            _orderRepository = orderRepository;
        }



        public IList<Product> FindAll()
        {
            return _productRepository.FindAll();
        }



        public Product FindBy(Guid id)
        {
            return _productRepository.FindBy(id);
        }



        public void Save(Product product)
        {
            if (product.GetBrokenRules().Count == 0)
            {
                _productRepository.Save(product);
            }
            else
            {
                String brokenRules = BuildErrorListFrom(product);

                throw new InvalidOperationException(
                    String.Format("This product can not be saved in this state. {0}", brokenRules));
            }
        }

        private String BuildErrorListFrom(Product product)
        {
            StringBuilder brokenRulesList = new StringBuilder();

            foreach (var brokenRule in product.GetBrokenRules())
            {
                brokenRulesList.Append(brokenRule);
                brokenRulesList.Append(Environment.NewLine);
            }

            return brokenRulesList.ToString();
        }




        public void Remove(Product product)
        {
            if (CanRemove(product))
            {
                _productRepository.Remove(product);
            }
            else
            {
                throw new ApplicationException(
                    "This product cannot be removed as it is linked to orders");
            }
        }

        private Boolean CanRemove(Product product)
        {
            return _orderRepository
                .FindAllOrdersContainingProductBy(product.Id)
                .Count == 0;
        }
    }
}
