﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;

namespace ACompany.OrderProcessing.Services
{
	public class CustomerService : ICustomerService
	{
		ICustomerRepository _customerRepository;

		public CustomerService(ICustomerRepository customerRepository)
		{
			_customerRepository = customerRepository;
		}



		public IList<Customer> FindAll()
		{
			return _customerRepository.FindAll();
		}

		
		public Customer FindBy(Guid id)
		{
			return _customerRepository.FindBy(id);
		}


		public void Save(Customer customer)
		{
			if (customer.GetBrokenRules().Count == 0)
			{
				_customerRepository.Save(customer);
			}
			else
			{
				String brokenRules = BuildErrorListFrom(customer);

				throw new InvalidOperationException(
					String.Format("This customer can ot be saved in this state. {0}", brokenRules));
			}
		}

		private String BuildErrorListFrom(Customer customer)
		{
			StringBuilder brokenRulesList = new StringBuilder();

			foreach (var brokenRule in customer.GetBrokenRules())
			{
				brokenRulesList.Append(brokenRule);
				brokenRulesList.Append(Environment.NewLine);
			}

			return brokenRulesList.ToString();
		}



		public void Remove(Customer customer)
		{
			if (CanRemove(customer))
			{
				_customerRepository.Remove(customer);
			}
			else
			{
				throw new ApplicationException(
					"Can not delete customer that has orders.");
			}
		}

		private Boolean CanRemove(Customer customer)
		{
			return customer.Orders.Count == 0;
		}


	}
}
