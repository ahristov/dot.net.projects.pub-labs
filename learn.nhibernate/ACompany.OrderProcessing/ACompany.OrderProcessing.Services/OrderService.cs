﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;
using ACompany.OrderProcessing.Infrastructure;
using ACompany.OrderProcessing.Infrastructure.Query;

namespace ACompany.OrderProcessing.Services
{
	public class OrderService : IOrderService
	{
		IOrderRepository _orderRepository;
		IProductRepository _productRepository;
		IUnitOfWork _unitOfWork;


		public OrderService(
			IOrderRepository orderRepository,
			IProductRepository productRepository,
			IUnitOfWork unitOfWork)
		{
			_orderRepository = orderRepository;
			_orderRepository.Inject(_unitOfWork);

			_productRepository = productRepository;
			_productRepository.Inject(_unitOfWork);

			_unitOfWork = unitOfWork;

		}


		public Order FindBy(Guid id)
		{
			return _orderRepository.FindBy(id);
		}

		public void Place(Order order)
		{
			_orderRepository.Save(order);
			_unitOfWork.Commit();
		}

		public IList<Order> GetAllOrdersBy(OrderStatus status)
		{
			Query orderStatusQuery = new Query();
			
			Criteria orderStatusCriteria = new Criteria(
				"Status", status, CriteriaOperator.Equals);

			orderStatusQuery.Criteria.Add(orderStatusCriteria);

			return _orderRepository.FindBy(orderStatusQuery);
		}

		public void Process(Order order)
		{
			if (order.CanProcess)
			{
				order.Process();

				foreach (Item item in order.Items)
				{
					item.Product.Stock -= item.Qty;
					_productRepository.Save(item.Product);
				}

				_orderRepository.Save(order);
				_unitOfWork.Commit();
			}
			else
			{
				throw new InvalidOperationException(
					"This order can not be processed");
			}
		}

		public void Save(Order order)
		{
			if (order.GetBrokenRules().Count == 0)
			{
				_orderRepository.Save(order);
				_unitOfWork.Commit();
			}
			else
			{
				String brokenRules = BuildErrorListFrom(order);
				throw new InvalidOperationException(String.Format(
					"The order can not be saved in this state, {0}",
					brokenRules));
			}
		}

		private String BuildErrorListFrom(Order order)
		{
			StringBuilder brokenRulesList = new StringBuilder();

			foreach (String brokenRule in order.GetBrokenRules())
			{
				brokenRulesList.Append(brokenRule);
				brokenRulesList.Append(Environment.NewLine);
			}

			return brokenRulesList.ToString();
		}
	}
}
