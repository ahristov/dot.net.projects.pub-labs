﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Infrastructure
{
    public interface IRepository<TAggregateRoot>
        where TAggregateRoot : IAggregateRoot
    {

        void Save(TAggregateRoot entity);
        void Remove(TAggregateRoot entity);

        TAggregateRoot FindBy(Guid Id);
		IList<TAggregateRoot> FindBy(Query.Query query);
        IList<TAggregateRoot> FindAll();

		void Inject(IUnitOfWork unitOfWork);
    }
}
