﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Infrastructure
{
    public interface IEntity
    {
        Guid Id { get; }
		Boolean IsTransient { get; }
    }
}
