﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Infrastructure
{
	public class ConcurrencyException : ApplicationException
	{
		public ConcurrencyException() 
			: base() { }

		public ConcurrencyException(String message)
			: base(message) { }

		public ConcurrencyException(String message, Exception innerException)
			: base(message, innerException) { }
	}
}
