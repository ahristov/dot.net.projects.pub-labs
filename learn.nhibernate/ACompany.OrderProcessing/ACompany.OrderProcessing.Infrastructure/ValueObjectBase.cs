﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Infrastructure
{
    public abstract class ValueObjectBase
    {
        protected abstract void Validate();
    }
}
