﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Infrastructure
{
	public interface IUnitOfWork
	{
		void Save(IAggregateRoot entity);
		void Delete(IAggregateRoot entity);
		void Commit();
	}
}
