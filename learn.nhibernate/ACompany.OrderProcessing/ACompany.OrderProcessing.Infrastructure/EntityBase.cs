﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace ACompany.OrderProcessing.Infrastructure
{
    public abstract class EntityBase<TEntity> : IEntity
        where TEntity : class, IEntity
    {
        private Guid _id;
        private List<String> _brokenRules = new List<string>();

        public Guid Id
        {
            get { return _id; }
            private set { _id = value; }
        }

		public bool IsTransient
		{
			get 
			{
				return _id == null;
			}
		}

		#region Equals

		public override bool Equals(object obj)
		{
			TEntity entityToCompare = obj as TEntity;

			if (obj == null)
			{
				// is the passed object is null, they cannot be equal
				return false;
			}

			if (entityToCompare == null)
			{
				// if the passed object is not TEntity, they cannot be equal
				return false;
			}

			if (this.HasSameIdentifierAs(entityToCompare))
			{
				// Both entities are persistent so we are able
				// to compare the identifiers
				return true;
			}
			else if (this.HasDifferentPersistanceContextAs(entityToCompare))
			{
				// One entity is transient on is
				// persistens so that cannot be equal
				return false;
			}
			else
			{
				// Neither entity has an identity
				return false;
			}

		}

		public override int GetHashCode()
		{
			if (this.IsTransient)
			{
				return this.GetType().FullName.GetHashCode();
			}
			else
			{
				return String.Format("{0}{1}",
					this.GetType().FullName, Id.ToString()).GetHashCode();
			}
		}

		private Boolean HasSameIdentifierAs(TEntity entityToCompare)
		{
			if (this.IsTransient || entityToCompare.IsTransient)
			{
				return false;
			}
			else
			{
				return this.Id == entityToCompare.Id;
			}
		}

		private Boolean HasDifferentPersistanceContextAs(TEntity entityToCompare)
		{
			return ! (this.IsTransient == entityToCompare.IsTransient);
		}


		#endregion Equals



		#region Validation

		protected abstract void Validate();

        protected List<String> BrokenRules
        {
            get { return _brokenRules; }
        }


        public ReadOnlyCollection<String> GetBrokenRules()
        {
            _brokenRules.Clear();
            Validate();
            return new ReadOnlyCollection<string>(_brokenRules);
		}


		#endregion Validation
	}
}
