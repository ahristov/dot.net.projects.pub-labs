﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Infrastructure.Query
{
	public class Query
	{
		IList<Criteria> _criteria;

		public Query()
		{
			_criteria = new List<Criteria>();
		}

		public IList<Criteria> Criteria
		{
			get { return _criteria; }
			set { _criteria = value; }
		}
	}
}
