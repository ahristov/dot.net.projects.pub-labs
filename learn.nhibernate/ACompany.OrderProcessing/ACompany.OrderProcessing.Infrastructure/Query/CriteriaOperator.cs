﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Infrastructure.Query
{
	public enum CriteriaOperator
	{
		Like = 1,
		Equals = 2,
	}
}
