﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Infrastructure.Query
{
	public class Criteria
	{
		String _property;
		Object _value;
		CriteriaOperator _criteriaOperator;

		public Criteria(String property, Object value, CriteriaOperator criteriaOperator)
		{
			_property = property;
			_value = value;
			_criteriaOperator = criteriaOperator;
		}

		public String PropertyName
		{
			get { return _property; }
			set { _property = value; }
		}

		public Object Value
		{
			get { return _value; }
			set { _value = value; }
		}

		public CriteriaOperator CriteriaOperator
		{
			get { return _criteriaOperator; }
			set { _criteriaOperator = value; }
		}
	}
}
