﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;

namespace ACompany.OrderProcessing.Presentation
{
	public interface ICustomerListView : IViewBase
	{
		IList<Customer> Customers { set; }
	}
}
