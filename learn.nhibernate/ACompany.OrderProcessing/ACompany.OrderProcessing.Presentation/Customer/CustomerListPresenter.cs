﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;
using ACompany.OrderProcessing.Services;

namespace ACompany.OrderProcessing.Presentation
{
	public class CustomerListPresenter : ICustomerListPresenter
	{
		ICustomerListView _view;
		ICustomerService _customerService;

		public CustomerListPresenter(
			ICustomerListView view,
			ICustomerService customerService)
		{
			_view = view;
			_customerService = customerService;
		}


		public void Display()
		{
			ClearInfoAndErrorMessages();
			
			IList<Customer> allCustomers = _customerService.FindAll();
			
			_view.Customers = allCustomers;

			_view.SetViewTitle("Customer List");
		}



		public void RemoveCustomerBy(Guid customerId)
		{
			ClearInfoAndErrorMessages();

			Customer customer = _customerService.FindBy(customerId);

			if (customer != null)
			{
				if (customer.Orders.Count == 0)
				{
					_customerService.Remove(customer);
					Display();
				}
				else
				{
					_view.SetErrorMessage("Cannot delete a customer that has orders.");
				}
			}
			else
			{
				_view.SetErrorMessage(String.Format("A customer with Id {0} cannot be found.",
					customerId));
			}
		}



		void ClearInfoAndErrorMessages()
		{
			_view.SetErrorMessage(String.Empty);
			_view.SetInfoMessage(String.Empty);
		}

	}
}
