﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Presentation
{
	public interface ICustomerPresenter : IPresenterBase
	{
		void SaveCustomer();
	}
}
