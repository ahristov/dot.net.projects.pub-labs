﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Presentation
{
	public interface ICustomerView : IViewBase
	{
		Guid CustomerId { get; }

		String Name { get; set; }
		String Email { get; set; }
		String Street { get; set; }
		String City { get; set; }
		String Country { get; set; }
		String PostCode { get; set; }

		String SaveButtonText { set; }
	}
}
