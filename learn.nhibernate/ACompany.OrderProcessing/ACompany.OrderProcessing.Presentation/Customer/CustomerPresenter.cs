﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACompany.OrderProcessing.Model;

namespace ACompany.OrderProcessing.Presentation
{
	public class CustomerPresenter : ICustomerPresenter 
	{
		ICustomerView _view;
		ICustomerService _customerService;
		Customer _selectedCustomer;
		Mode _mode;

		public CustomerPresenter(
			ICustomerView view,
			ICustomerService customerService)
		{
			_view = view;
			_customerService = customerService;
		}



		public void Display()
		{
			Customer customer = GetSelectedCustomerDetail();
			ClearInfoAndErrorMessages();
			SetButtonText();
			SetTitleText();
			if (customer != null)
			{
				UpdateView(customer);
			}
			else
			{
				_view.SetErrorMessage(String.Format("Could not find details from Customer with Id of {0}",
					_view.CustomerId));
			}
		}



		void SetButtonText()
		{
			if (_mode == Mode.Add)
			{
				_view.SaveButtonText = "Add Customer";
			}
			else
			{
				_view.SaveButtonText = "Save Customer";
			}
		}

		void SetTitleText()
		{
			if (_mode == Mode.Add)
			{
				_view.SetViewTitle("Add Customer");
			}
			else
			{
				_view.SetViewTitle("Edit Customer");
			}
		}

		void ClearInfoAndErrorMessages()
		{
			_view.SetErrorMessage(String.Empty);
			_view.SetInfoMessage(String.Empty);
		}


		Customer GetSelectedCustomerDetail()
		{
			if (_selectedCustomer == null)
			{
				if (_view.CustomerId == Guid.Empty)
				{
					_selectedCustomer = new Customer();
					_mode = Mode.Add;
				}
				else
				{
					_selectedCustomer = _customerService.FindBy(_view.CustomerId);
					_mode = Mode.Edit;
				}
			}

			return _selectedCustomer;
		}

		void UpdateView(Customer customer)
		{
			_view.Name = customer.Name;
			_view.Email = customer.Email;

			if (customer.Address != null)
			{
				_view.Street = customer.Address.Street;
				_view.City = customer.Address.City;
				_view.Country = customer.Address.Country;
				_view.PostCode = customer.Address.PostCode;
			}
			else
			{
				_view.Street = String.Empty;
				_view.City = String.Empty;
				_view.Country = String.Empty;
				_view.PostCode = String.Empty;
			}
		}





		public void SaveCustomer()
		{
			Customer customer = GetSelectedCustomerDetail();
			ClearInfoAndErrorMessages();

			try
			{
				UpdateFromView(customer);
				if (customer.GetBrokenRules().Count == 0)
				{
					try
					{
						_customerService.Save(customer);
						if (_mode == Mode.Add)
						{
							_view.SetInfoMessage(String.Format("Customer '{0}' has been added.",
								customer.Name));
							ClearView();
						}
						else
						{
							_view.SetInfoMessage("Customer has been saved.");
						}
					}
					catch (Exception ex)
					{
						string msg = String.Format("A problem occured while saving customer '{0}'. {1}{2}",
							customer.Name,
							Environment.NewLine,
							ex.Message);
						_view.SetErrorMessage(msg);
					}
				}
				else
				{
					DisplayErrorsIn(customer);
				}
			}
			catch (Exception ex)
			{
				_view.SetErrorMessage(ex.Message);
			}
		}


		void UpdateFromView(Customer customer)
		{
			customer.Name = _view.Name;
			customer.Email = _view.Email;

			Address address = new Address(_view.Street, _view.City, _view.Country, _view.PostCode);
			customer.Address = address;
		}

		void ClearView()
		{
			_view.Name = String.Empty;
			_view.Email = String.Empty;
			_view.Street = String.Empty;
			_view.City = String.Empty;
			_view.Country = String.Empty;
			_view.PostCode = String.Empty;
		}


		void DisplayErrorsIn(Customer customer)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("There were some problems when trying to save this Customer:");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			foreach (String brokenRule in customer.GetBrokenRules())
			{
				sb.Append(brokenRule);
				sb.Append(Environment.NewLine);
			}

			_view.SetErrorMessage(sb.ToString());
		}
	}
}
