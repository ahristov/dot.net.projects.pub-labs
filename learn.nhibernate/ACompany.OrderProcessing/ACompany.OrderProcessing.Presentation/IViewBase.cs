﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACompany.OrderProcessing.Presentation
{
	public interface IViewBase
	{
		void SetErrorMessage(String message);
		void SetInfoMessage(String message);
		void SetViewTitle(String title);
	}
}
