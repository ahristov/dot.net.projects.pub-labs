﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHibernate.QuickDemo.NHibernateDAL;

namespace NHibernate.QuickDemo.Web
{
    public partial class Default : System.Web.UI.Page
    {
        SessionFactory _sessionFactory;
        PersonService _personService;

        protected void Page_Init(object sender, EventArgs e)
        {
            _sessionFactory = new SessionFactory();
            _personService = new PersonService(_sessionFactory);
        }

        protected override void OnUnload(EventArgs e)
        {

            base.OnUnload(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayAllPeople();
            }
        }

        public void DisplayAllPeople()
        {
            this.grdPeople.DataSource = _personService.GetAllPeople();
            this.grdPeople.DataBind();
        }

        protected void btnAddPerson_Click(Object sender, System.EventArgs e)
        {
            Person aNewPerson = new Person();
            
            aNewPerson.FirstName = this.txtFirstName.Text;
            aNewPerson.LastName = this.txtLastName.Text;
            aNewPerson.DOB = this.calDOB.SelectedDate;

            _personService.Add(aNewPerson);

            DisplayAllPeople();
        }

    }
}