﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NHibernate.QuickDemo.Web.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NHibernate Quick Demo</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    

<h2>All People</h2>

<asp:GridView ID="grdPeople" runat="server" />

<br />
<br />

First Name:
<asp:TextBox ID="txtFirstName" runat="server" /><br />

Last Name:
<asp:TextBox ID="txtLastName" runat="server" /><br />

DOB:
<asp:Calendar ID="calDOB" runat="server" /><br />

<asp:Button ID="btnAddPerson" runat="server" Text="Add Person"  OnClick="btnAddPerson_Click" />



    </div>
    </form>
</body>
</html>
