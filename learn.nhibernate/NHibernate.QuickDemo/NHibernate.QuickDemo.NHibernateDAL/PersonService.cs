﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//
// On ASP.Net better inject ISession instead of using these using() statements.
// Create ISession object for the live of the web request.
// See: https://github.com/sharparchitecture/sharp-architecture/wiki
//

namespace NHibernate.QuickDemo.NHibernateDAL
{
    public class PersonService
    {
        SessionFactory _sessionFactory;

        public PersonService(SessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }


        public IList<Person> GetAllPeople()
        {
            IList<Person> allPeople = new List<Person>();

            using (NHibernate.ISession session = _sessionFactory.GetNewSession())
            {
                NHibernate.ICriteria qry = session.CreateCriteria<Person>();
                allPeople = qry.List<Person>();
            }

            return allPeople;
        }

        public void Add(Person person)
        {
            using (NHibernate.ISession session = _sessionFactory.GetNewSession())
            {
                using (NHibernate.ITransaction tx = session.BeginTransaction())
                {
                    session.Save(person);
                    tx.Commit();
                }
            }
        }

    }
}
