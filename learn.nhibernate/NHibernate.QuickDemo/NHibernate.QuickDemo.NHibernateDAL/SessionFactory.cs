﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace NHibernate.QuickDemo.NHibernateDAL
{
    public class SessionFactory
    {
        private static NHibernate.ISessionFactory _sessionFactory;

        private static void Init()
        {
            NHibernate.Cfg.Configuration config;
            config = new NHibernate.Cfg.Configuration();
            config.Configure();

            // Ensure mapping files are embeded in the assembly
            // config.AddAssembly("NHibernate.QuickDemo.NHibernateDAL");
            config.AddAssembly(Assembly.GetExecutingAssembly());
            _sessionFactory = config.BuildSessionFactory();
        }

        private static NHibernate.ISessionFactory GetSessionFactory()
        {
            if (_sessionFactory == null)
            {
                Init();
            }

            return _sessionFactory;
        }

        public NHibernate.ISession GetNewSession()
        {
            return GetSessionFactory().OpenSession();
        }
    }
}
