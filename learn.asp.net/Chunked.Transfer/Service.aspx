﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<%@ Import Namespace="System.Threading" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.BufferOutput = false;
        Response.AddHeader("Pragma", "no-cache");
        Response.ContentType = "text/plain";

        for (int i = 1; i <= 60 * 60; i++)
        {
            Response.Write(String.Format("{{ ReturnValue:{0}, AnotherValue:\"{0}\" }}\r\n", i));
            Thread.Sleep(1000);
        }

        Response.End();
    }

</script>
