﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace RefactoringPatterns.ExtractMethodObject.Old
{
    //////////////////////////////////////////////////////////////////////
    #region ExtractMethodObject - Old

    class OrderLineItem
    {
        public decimal Price { get; private set; }
    }

    class Order
    {
        private List<OrderLineItem> _orderLineItems = new List<OrderLineItem>();
        public ReadOnlyCollection<OrderLineItem> OrderLines
        {
            get { return new ReadOnlyCollection<OrderLineItem>(_orderLineItems); }
        }

        private List<decimal> _discounts = new List<decimal>();
        public ReadOnlyCollection<decimal> Discounts
        {
            get { return new ReadOnlyCollection<decimal>(_discounts); }
        }

        private decimal _tax = 0;

        public decimal Calculate()
        {
            decimal subTotal = 0m;
            foreach (OrderLineItem lineItem in _orderLineItems)
            {
                subTotal += lineItem.Price;
            }

            foreach (decimal discount in _discounts)
            {
                subTotal -= discount;
            }

            decimal tax = subTotal * _tax;

            decimal grandTotal = subTotal + tax;

            return grandTotal;
        }
    }

    #endregion ExtractMethodObject - Old
    //////////////////////////////////////////////////////////////////////
}




namespace RefactoringPatterns.ExtractMethodObject.New
{
    //////////////////////////////////////////////////////////////////////
    #region ExtractMethodObject - New

    class OrderLineItem
    {
        public decimal Price { get; private set; }
    }

    class Order
    {
        private List<OrderLineItem> _orderLineItems = new List<OrderLineItem>();
        public ReadOnlyCollection<OrderLineItem> OrderLineItems
        {
            get { return new ReadOnlyCollection<OrderLineItem>(_orderLineItems); }
        }

        private List<decimal> _discounts = new List<decimal>();
        public ReadOnlyCollection<decimal> Discounts
        {
            get { return new ReadOnlyCollection<decimal>(_discounts); }
        }

        public decimal Tax { get; private set; }

        public decimal Calculate()
        {
            return new OrderCalculator(this).Calculate();

            /*
            decimal subTotal = 0m;
            foreach (OrderLineItem lineItem in _orderLineItems)
            {
                subTotal += lineItem.Price;
            }

            foreach (decimal discount in _discounts)
            {
                subTotal -= discount;
            }

            decimal tax = subTotal * _tax;

            decimal grandTotal = subTotal + tax;

            return grandTotal;
             * */
        }
    }

    class OrderCalculator
    {
        decimal _subTotal;
        ReadOnlyCollection<OrderLineItem> _orderLineItems;
        ReadOnlyCollection<decimal> _discounts;
        decimal _tax;

        public OrderCalculator(Order order)
        {
            _subTotal = 0;
            _orderLineItems = order.OrderLineItems;
            _discounts = order.Discounts;
            _tax = order.Tax;
        }

        public decimal Calculate()
        {
            _subTotal = 0;

            CalculateSubTotal();

            SubstractDiscounts();

            CalculateTax();

            return _subTotal;
        }

        private void CalculateSubTotal()
        {
            foreach (OrderLineItem lineItem in _orderLineItems)
            {
                _subTotal += lineItem.Price;
            }
        }

        private void SubstractDiscounts()
        {
            foreach (decimal discount in _discounts)
            {
                _subTotal -= discount;
            }
        }

        private void CalculateTax()
        {
            _subTotal += _subTotal * _tax;
        }
    }

    #endregion ExtractMethodObject - New
    //////////////////////////////////////////////////////////////////////
}
