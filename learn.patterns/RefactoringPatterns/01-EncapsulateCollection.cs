﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace RefactoringPatterns.EncapsulateCollection
{
    //////////////////////////////////////////////////////////////////////
    #region Encapsulate Collection

    class OrderLine { }

    class Order
    {
        List<OrderLine> _orderLines = new List<OrderLine>();

        public ReadOnlyCollection<OrderLine> OrderLines
        {
            get { return new ReadOnlyCollection<OrderLine>(_orderLines); }
        }

        // -or -

        /*
        // This can be converted to ICollection and modified
        public IEnumerable<OrderLine> OrderLines
        {
            // prefer Skip(0) over Where(func) because not running a function for each member
            get { return _orderLines.Skip(0); }
        }
        */

        public void AddOrderLine(OrderLine orderLine) { _orderLines.Add(orderLine); }

        public void RemoveOrderLine(OrderLine orderLine)
        {
            orderLine = _orderLines.Find(ol => ol == orderLine);
            if (orderLine == null) { return; }

            _orderLines.Remove(orderLine);
        }
    }

    #endregion Encapsulate Collection
    //////////////////////////////////////////////////////////////////////
}
