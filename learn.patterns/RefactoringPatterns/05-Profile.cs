﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace RefactoringPatterns.Profile
{
    /*
     * Usage Example:
     *   
     *      String outputFileName = @"..\\..\\PerformanceTestsData.csv";
     *
     *      try { File.Delete(outputFileName); }
     *       catch { }
     *       new Profile.RunTest(outputFileName).Run(typeof(Profile.RunnerOne));
     *       new Profile.RunTest(outputFileName).Run(typeof(Profile.RunnerTwo));
     *
     */

    public class TimeIt : IDisposable
    {
        readonly Stopwatch _stopwatch = new Stopwatch();
        readonly Int32 _total;
        readonly String _testName;
        readonly Type _type;

        public TimeIt(int total, string testName, Type type)
        {
            _total = total;
            _testName = testName;
            _type = type;
            _stopwatch.Start();
        }

        public void  Dispose()
        {
 	        _stopwatch.Stop();
            Trace.WriteLine(string.Format("{0}, {1}, {2}, {3}", _testName, _type.Name, _total, _stopwatch.ElapsedMilliseconds));
        }
    }




    public interface IRunner { void Run(); }

    public class RunnerOne : IRunner
    {
        public void Run()
        {
            Thread.Sleep(5);
        }
    }

    public class RunnerTwo : IRunner
    {
        public void Run()
        {
            Thread.Sleep(6);
        }
    }




    public class RunTest
    {
        private readonly int[] _totals = { 1, 10, 100, 200, 500, 1000 };
        private static DelimitedListTraceListener _listener;

        public RunTest(String outputFileName)
        {
            if (_listener == null)
            {
                _listener = new DelimitedListTraceListener(outputFileName);
            }

            Trace.Listeners.Clear();
            Trace.Listeners.Add(_listener);
            Trace.AutoFlush = true;
        }

        public void Run(Type type)
        {
            IRunner runner = null;
            if (type == typeof(RunnerOne)) { runner = new RunnerOne(); }
            else if (type == typeof(RunnerTwo)) { runner = new RunnerTwo(); }

            foreach (var total in _totals)
            {
                using (new TimeIt(total, "TestingRunMethod", type))
                {
                    for (int i = 0; i < total; i++)
                    {
                        runner.Run();
                    }
                }
            }

        }
    }






}
