﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefactoringPatterns.BreakDependenciesToInterface.Old
{
    //////////////////////////////////////////////////////////////////////
    #region Break Dependencies To Interface - Old

    class AnimalFeedingService
    {
        private bool _foodBowlEmpty = false;

        public void Feed()
        {
            if (_foodBowlEmpty) { Feeder.ReplenishFood(); }
        }
    }

    static class Feeder
    {
        public static void ReplenishFood() { }
    }

    #endregion Break Dependencies To Interface - Old
    //////////////////////////////////////////////////////////////////////
}

namespace RefactoringPatterns.BreakDependenciesToInterface.New
{
    //////////////////////////////////////////////////////////////////////
    #region Break Dependencies To Interface - New

    class AnimalFeedingService
    {
        IFeederService _feederService;
        private bool _foodBowlEmpty;

        AnimalFeedingService(IFeederService feedingService)
        {
            _feederService = feedingService;
            _foodBowlEmpty = false;
        }

        public void Feed()
        {
            if (_foodBowlEmpty) { _feederService.ReplenishFood(); }
        }
    }

    interface IFeederService
    {
        void ReplenishFood();
    }
    class FeederService : IFeederService
    {
        public void ReplenishFood()
        {
            Feeder.ReplenishFood();
        }
    }


    static class Feeder
    {
        public static void ReplenishFood() { }
    }

    #endregion Break Dependencies To Interface - New
    //////////////////////////////////////////////////////////////////////

}
