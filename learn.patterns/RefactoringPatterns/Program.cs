﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.IO;

namespace RefactoringPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            String outputFileName = @"..\\..\\PerformanceTestsData.csv";

            try { File.Delete(outputFileName); }
            catch { }
            new Profile.RunTest(outputFileName).Run(typeof(Profile.RunnerOne));
            new Profile.RunTest(outputFileName).Run(typeof(Profile.RunnerTwo));
        }
    }
}
