﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefactoringPatterns.StrategyPattern
{

    //////////////////////////////////////////////////////////////////////
    #region Switch to Strategy

    class ClientCode
    {
        // [Inject]
        public IShippingInfo ShippingInfo { get; set; }

        public decimal CalculateShipping()
        {
            return ShippingInfo.CalculateShippingAmount(State.Alaska);
        }
    }

    enum State
    {
        Alaska,
        NewYork,
        Florida,
    }

    interface IShippingCalculation
    {
        State State { get; }
        decimal Calculate();
    }
    class AlaskaShippingCalculation : IShippingCalculation
    {
        public State State { get { return State.Alaska; } }
        public decimal Calculate() { return 15m; }
    }
    class NewYorkShippingCalculation : IShippingCalculation
    {
        public State State { get { return State.NewYork; } }
        public decimal Calculate() { return 10m; }
    }
    class FloridaShippingCalculation : IShippingCalculation
    {
        public State State { get { return State.Florida; } }
        public decimal Calculate() { return 10m; }
    }

    interface IShippingInfo
    {
        decimal CalculateShippingAmount(State shipToState);
    }

    class ShippingInfo : IShippingInfo
    {
        private Dictionary<State, IShippingCalculation> _shippingCalculations;

        public ShippingInfo(IEnumerable<IShippingCalculation> shippingCalculations)
        {
            _shippingCalculations = shippingCalculations.ToDictionary(calc => calc.State);
        }

        public decimal CalculateShippingAmount(State shipToState)
        {
            return _shippingCalculations[shipToState].Calculate();
        }
    }

    #endregion Switch to Strategy
    //////////////////////////////////////////////////////////////////////


}
