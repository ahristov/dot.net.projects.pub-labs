﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using DBAccessPerf.Tests;
using System.IO;
using DBAccessPerf.Helpers;
using DBAccessPerf.Entities;

namespace DBAccessPerf
{
    class Program
    {
        static void Main(string[] args)
        {
			// Init just once at the very beginning to avoid side effects later
			ConnectionFactory.Init();
			NHibernateSessionFactory.Init();

			//String customerName = new TestSelectOnOneTableNHibernateWithHQL().Run(1);
			//Console.WriteLine(customerName);
			

			String outputFileName = @"..\\..\\DBAccessPerfData.csv";

			try { File.Delete(outputFileName); }
			catch { }

            int countRuns = 1000;

			//new TestRunner(outputFileName, countRuns).DoForTestRunnerWithJoin("Join on 3 tables", typeof(TestJoinOnThreeTablesADOReader));
			//new TestRunner(outputFileName, countRuns).DoForTestRunnerWithJoin("Join on 3 tables", typeof(TestJoinOnThreeTablesADODataTable));
			//new TestRunner(outputFileName, countRuns).DoForTestRunnerWithJoin("Join on 3 tables", typeof(TestJoinOnThreeTablesDapper));
            //new TestRunner(outputFileName, countRuns).DoForTestRunnerWithJoin("Join on 3 tables", typeof(TestJoinOnThreeTablesNHibernateWithCriteria));
            //new TestRunner(outputFileName, countRuns).DoForTestRunnerWithJoin("Join on 3 tables", typeof(TestJoinOnThreeTablesNHibernateWithProjections));
            //new TestRunner(outputFileName, countRuns).DoForTestRunnerWithJoin("Join on 3 tables", typeof(TestJoinOnThreeTablesNHibernateWithHQL));

            //new TestRunner(outputFileName, countRuns).DoForTestRunnerWithTotal("Totals on 1 table", typeof(TestTotalsOnOneTableADOReader));
            //new TestRunner(outputFileName, countRuns).DoForTestRunnerWithTotal("Totals on 1 table", typeof(TestTotalsOnOneTableADODataTable));
            new TestRunner(outputFileName, countRuns).DoForTestRunnerWithTotal("Totals on 1 table", typeof(TestTotalsOnOneTableDapper));
            //new TestRunner(outputFileName, countRuns).DoForTestRunnerWithTotal("Totals on 1 table", typeof(TestTotalsOnOneTableNHibernateWithProjections));
            //new TestRunner(outputFileName, countRuns).DoForTestRunnerWithTotal("Totals on 1 table", typeof(TestTotalsOnOneTableNHibernateWithHQL));

			//new TestRunner(outputFileName, countRuns).DoForTestRunnerWithSelect("Select from 1 table", typeof(TestSelectOnOneTableADOReader));
			//new TestRunner(outputFileName, countRuns).DoForTestRunnerWithSelect("Select from 1 table", typeof(TestSelectOnOneTableADODataTable));
			//new TestRunner(outputFileName, countRuns).DoForTestRunnerWithSelect("Select from 1 table", typeof(TestSelectOnOneTableDapper));
            //new TestRunner(outputFileName, countRuns).DoForTestRunnerWithSelect("Select from 1 table", typeof(TestSelectOnOneTableNHibernateWithCriteria));
            //new TestRunner(outputFileName, countRuns).DoForTestRunnerWithSelect("Select from 1 table", typeof(TestSelectOnOneTableNHibernateWithHQL));
        }


        #region Trying some things and usage

        static void NHibernateUsageExample()
		{
			using (var session = new NHibernateSessionFactory().GetNewSession())
			{

				SubscriptionType st;
				st = session.Get<SubscriptionType>(1);

				using (var tx = session.BeginTransaction())
				{
					st = session.Get<SubscriptionType>(1);
					tx.Commit();
				}



				NHibernate.ICriteria qry;

				// get all the subscription types
				qry = session.CreateCriteria<SubscriptionType>();
				IList<SubscriptionType> lstST = qry.List<SubscriptionType>();

				// get first 5 customers, their orders are lazy loaded when needed
				qry = session.CreateCriteria<Customer>()
					.Add(NHibernate.Criterion.Expression.Lt("Id", 5)); ;
				IList<Customer> lstC = qry.List<Customer>();

				// get first 50 orders, their customers or subscription types are lazy loaded when needed
				qry = session.CreateCriteria<CustomerOrder>()
					.Add(NHibernate.Criterion.Expression.Lt("Id", 50));
				IList<CustomerOrder> lstCO = qry.List<CustomerOrder>();
			}
        }


        #endregion Trying some things and usage
    }



}
