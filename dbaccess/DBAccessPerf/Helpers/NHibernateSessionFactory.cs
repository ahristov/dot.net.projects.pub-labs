﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;
using System.Reflection;

namespace DBAccessPerf.Helpers
{
	public class NHibernateSessionFactory
	{
		private static ISessionFactory sessionFactory;

		public static void Init()
		{
			NHibernate.Cfg.Configuration config;
			config = new NHibernate.Cfg.Configuration();
			config.Configure();

			config.AddAssembly(Assembly.GetExecutingAssembly());
			sessionFactory = config.BuildSessionFactory();
		}

		private static NHibernate.ISessionFactory GetSessionFactory()
		{
			if (sessionFactory == null)
			{
				Init();
			}

			return sessionFactory;
		}

		
		// get new nHibernate session object

		public NHibernate.ISession GetNewSession()
		{
			return GetSessionFactory().OpenSession();
		}


	}
}
