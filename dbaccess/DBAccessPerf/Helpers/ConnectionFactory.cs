﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace DBAccessPerf.Helpers
{
	public class ConnectionFactory
	{
		private static string cnnString = null;

		public static void Init()
		{
			ConnectionStringSettingsCollection connections =
				ConfigurationManager.ConnectionStrings;

			cnnString = connections["DBAccessPerf"].ConnectionString;
		}

		private static string GetConnectionString()
		{
			if (cnnString == null)
			{
				Init();
			}

			return cnnString;
		}

		// get new ADO.NET sql connection

		public static SqlConnection GetNewConnection()
		{
			var connection = new SqlConnection(GetConnectionString());
			connection.Open();
			return connection;
		}

	}
}
