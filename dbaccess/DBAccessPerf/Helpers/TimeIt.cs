﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace DBAccessPerf.Helpers
{
	public class TimeIt : IDisposable
	{
		readonly Stopwatch _stopwatch = new Stopwatch();
		readonly String _testName;
		readonly Type _type;

		public TimeIt(string testName, Type type)
		{
			_testName = testName;
			_type = type;
			_stopwatch.Start();
		}

		//public void Stop()
		//{
		//    _stopwatch.Stop();
		//    Trace.WriteLine(string.Format("{0}, {1}, {2}", _testName, _type.Name, _stopwatch.ElapsedMilliseconds));
		//}

		public void Dispose()
		{
			_stopwatch.Stop();
			Trace.WriteLine(string.Format("{0}, {1}, {2}", _testName, _type.Name, _stopwatch.ElapsedMilliseconds));
		}
	}
}
