﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccessPerf.DTO
{
	public class OrderSelection
	{
		// DTO, ValueObject

		public Int32 CustomerId { get; set; }
		public String CustomerName { get; set; }
		public Int32 CustomerOrderId { get; set; }
		public Decimal PriceGross { get; set; }
		public String SubscriptionTypeName { get; set; }


	}
}
