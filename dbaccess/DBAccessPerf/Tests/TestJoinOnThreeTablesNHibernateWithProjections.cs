﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.DTO;
using DBAccessPerf.Helpers;
using DBAccessPerf.Entities;


namespace DBAccessPerf.Tests
{
	public class TestJoinOnThreeTablesNHibernateWithProjections : ITestRunnerWithJoin
	{
		public IEnumerable<OrderSelection> Run(int customerID)
		{
			using (var session = new NHibernateSessionFactory().GetNewSession())
			{
				NHibernate.ICriteria qry = session.CreateCriteria<CustomerOrder>("co")
					.CreateAlias("Customer", "c")
					.CreateAlias("SubscriptionType", "st")
					.SetProjection(NHibernate.Criterion.Projections.ProjectionList()
						.Add(NHibernate.Criterion.Projections.Property("c.Id"), "CustomerId")
						.Add(NHibernate.Criterion.Projections.Property("c.Name"), "CustomerName")
						.Add(NHibernate.Criterion.Projections.Property("co.Id"), "CustomerOrderId")
						.Add(NHibernate.Criterion.Projections.Property("co.PriceGross"), "PriceGross")
						.Add(NHibernate.Criterion.Projections.Property("st.Name"), "SubscriptionTypeName")
					)
					.Add(NHibernate.Criterion.Expression.Eq("c.Id", customerID));

				qry.SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<OrderSelection>());
				return qry.List<OrderSelection>();
			}
		}
	}
}
