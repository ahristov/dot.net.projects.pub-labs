﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.Helpers;
using System.Data.SqlClient;
using DBAccessPerf.DTO;
using Dapper;

namespace DBAccessPerf.Tests
{
    public class TestSelectOnOneTableDapper : ITestRunnerWithSelect
	{
		public String Run(int customerID)
		{
			String sql = "";
			String customerName = "";

			sql += "SELECT	customer_name						\n";
			sql += "FROM	fpo_customers						\n";
			sql += "WHERE	customer_id = " + customerID + "	\n";

			using (var cnn = ConnectionFactory.GetNewConnection())
			{
                customerName = cnn.Query<string>(sql).FirstOrDefault();
			}

			return customerName;
		}
	}
}
