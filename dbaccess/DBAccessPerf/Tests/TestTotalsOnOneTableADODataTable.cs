﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.Helpers;
using System.Data.SqlClient;
using DBAccessPerf.DTO;
using System.Data;


namespace DBAccessPerf.Tests
{
	public class TestTotalsOnOneTableADODataTable : ITestRunnerWithTotals
	{
		public void Run(int customerID, out decimal countOfOrders, out decimal totalGrossAmount)
		{
			countOfOrders = 0;
			totalGrossAmount = 0;
			string sql = "";

			sql += "SELECT		COUNT(*) AS count, SUM(price_gross) AS total		\n";
			sql += "FROM		fpo_customer_orders									\n";
			sql += "WHERE		(customer_id = "+customerID+")						\n";
			sql += "			AND (is_trash = 0)									\n";

			using (var cnn = ConnectionFactory.GetNewConnection())
			{
				var command = new SqlCommand(sql, cnn);
				var adapter = new SqlDataAdapter(command);
				var dataSet = new DataSet();

				adapter.Fill(dataSet, "TotalsSelection");

				var dataTable = dataSet.Tables["TotalsSelection"];

				foreach (DataRow dr in dataTable.Rows)
				{
					decimal.TryParse("" + dr["count"], out countOfOrders);
					decimal.TryParse("" + dr["total"], out totalGrossAmount);

					break;
				}
			}
		}
	}
}
