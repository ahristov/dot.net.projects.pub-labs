﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.DTO;
using DBAccessPerf.Helpers;
using DBAccessPerf.Entities;
using NHibernate.Criterion;
using NHibernate.Transform;


namespace DBAccessPerf.Tests
{
	public class TestTotalsOnOneTableNHibernateWithHQL : ITestRunnerWithTotals
	{
		public void Run(int customerID, out decimal countOfOrders, out decimal totalGrossAmount)
		{
			countOfOrders = 0;
			totalGrossAmount = 0;

			string hql = "";

			hql += "SELECT 	count(co.Id) as count,					\n";
			hql += "		sum(co.PriceGross) as total 			\n";
			hql += "FROM 	Customer as c					 		\n";
			hql += "			inner join c.Orders as co			\n";
			hql += "WHERE 	c.Id = :customerID						\n";
			hql += "		and co.IsTrash = false					\n";

			using (var session = new NHibernateSessionFactory().GetNewSession())
			{
				object[] data = session.CreateQuery(hql)
					.SetInt32("customerID", customerID)
					.List<object[]>().FirstOrDefault();

				decimal.TryParse("" + data[0], out countOfOrders);
				decimal.TryParse("" + data[1], out totalGrossAmount);
			}
		}
	}
}
