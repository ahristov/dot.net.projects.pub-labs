﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.Helpers;
using System.Data.SqlClient;
using DBAccessPerf.DTO;
using System.Data;


namespace DBAccessPerf.Tests
{
	public class TestSelectOnOneTableADODataTable : ITestRunnerWithSelect
	{
		public string Run(int customerID)
		{
			String sql = "";
			String customerName = "";

			sql += "SELECT	customer_name						\n";
			sql += "FROM	fpo_customers						\n";
			sql += "WHERE	customer_id = " + customerID + "	\n";

			using (var cnn = ConnectionFactory.GetNewConnection())
			{
				var command = new SqlCommand(sql, cnn);
				var adapter = new SqlDataAdapter(command);
				var dataSet = new DataSet();

				adapter.Fill(dataSet, "NameSelection");

				var dataTable = dataSet.Tables["NameSelection"];

				foreach (DataRow dr in dataTable.Rows)
				{
					customerName = "" + dr["customer_name"];

					break;
				}
			}

			return customerName;
		}
	}
}
