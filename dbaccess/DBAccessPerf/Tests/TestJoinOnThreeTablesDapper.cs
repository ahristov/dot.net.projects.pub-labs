﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.Helpers;
using System.Data.SqlClient;
using DBAccessPerf.DTO;
using Dapper;
using DBAccessPerf.Entities;


namespace DBAccessPerf.Tests
{
	public class TestJoinOnThreeTablesDapper : ITestRunnerWithJoin
	{
		public IEnumerable<OrderSelection> Run(int customerID)
		{
			string sql = "";

            sql += "SELECT      c.customer_id as Id,                                                                                \n";
            sql += "            c.customer_name as Name,                                                                            \n";
            sql += "            co.customer_order_id as Id,                                                                         \n";
            sql += "            co.price_gross as PriceGross,                                                                       \n";
            sql += "            st.subscription_type_id as Id,                                                                      \n"; // needed for default splitOn parameter to cnn.Query()
            sql += "            st.subscription_type_name as Name                                                                   \n";
            sql += "FROM        fpo_customers AS c                                                                                  \n";
            sql += "				join fpo_customer_orders AS co ON co.customer_id = c.customer_id                                \n";
            sql += "				join fpo_subscription_types_lookup AS st ON st.subscription_type_id = co.subscription_type_id   \n";
            sql += "WHERE       (co.is_trash = 0) AND (c.customer_id = "+customerID+")											    \n";
            sql += "ORDER BY	co.customer_order_id																			    \n";
            
            using (var cnn = ConnectionFactory.GetNewConnection())
			{

                IEnumerable<OrderSelection> orders = cnn.Query<Customer, CustomerOrder, SubscriptionType, OrderSelection>(sql,
                    (c, co, st) =>
                    {
                        return new OrderSelection
                        {
                            CustomerId = c.Id,
                            CustomerName = c.Name,
                            CustomerOrderId = co.Id,
                            PriceGross = co.PriceGross,
                            SubscriptionTypeName = st.Name,
                        };
                    });

                return orders;
			}

		}
	}
}
