﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.DTO;

namespace DBAccessPerf.Tests
{
    public interface ITestRunnerWithTotals
	{
		void Run(int customerID, out decimal countOfOrders, out decimal totalGrossAmount);
	}
}
