﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.Helpers;
using System.Data.SqlClient;
using DBAccessPerf.DTO;


namespace DBAccessPerf.Tests
{
	public class TestSelectOnOneTableADOReader : ITestRunnerWithSelect
	{
		public String Run(int customerID)
		{
			String sql = "";
			String customerName = "";

			sql += "SELECT	customer_name						\n";
			sql += "FROM	fpo_customers						\n";
			sql += "WHERE	customer_id = " + customerID + "	\n";

			using (var cnn = ConnectionFactory.GetNewConnection())
			{
				var command = new SqlCommand(sql, cnn);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						customerName = "" + reader["customer_name"];

						break;
					}
				}
			}

			return customerName;
		}
	}
}
