﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.DTO;
using DBAccessPerf.Helpers;
using DBAccessPerf.Entities;


namespace DBAccessPerf.Tests
{
	public class TestJoinOnThreeTablesNHibernateWithCriteria : ITestRunnerWithJoin
	{
		public IEnumerable<OrderSelection> Run(int customerID)
		{
			using (var session = new NHibernateSessionFactory().GetNewSession())
			{
				NHibernate.ICriteria qry = session.CreateCriteria<CustomerOrder>("co")
					.CreateAlias("Customer", "c")
					.Add(NHibernate.Criterion.Expression.Eq("c.Id", customerID));

				IList<CustomerOrder> customerOrders = qry.List<CustomerOrder>();

				foreach (var customerOrder in qry.List<CustomerOrder>())
				{
					yield return new OrderSelection
					{
						CustomerId = customerOrder.Customer.Id,
						CustomerName = customerOrder.Customer.Name,
						CustomerOrderId = customerOrder.Id,
						PriceGross = customerOrder.PriceGross,
						SubscriptionTypeName = customerOrder.SubscriptionType.Name
					};
				}
			}
		}
	}
}
