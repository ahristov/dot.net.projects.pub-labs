﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.Helpers;
using System.Data.SqlClient;
using DBAccessPerf.DTO;


namespace DBAccessPerf.Tests
{
	public class TestTotalsOnOneTableADOReader : ITestRunnerWithTotals
	{
		public void Run(int customerID, out decimal countOfOrders, out decimal totalGrossAmount)
		{
			countOfOrders = 0;
			totalGrossAmount = 0;
			string sql = "";

			sql += "SELECT		COUNT(*) AS count, SUM(price_gross) AS total		\n";
			sql += "FROM		fpo_customer_orders									\n";
			sql += "WHERE		(customer_id = "+customerID+")						\n";
			sql += "			AND (is_trash = 0)									\n";

			using (var cnn = ConnectionFactory.GetNewConnection())
			{
				var command = new SqlCommand(sql, cnn);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						decimal.TryParse("" + reader["count"], out countOfOrders);
						decimal.TryParse("" + reader["total"], out totalGrossAmount);

						break;
					}
				}
			}
		}
	}
}
