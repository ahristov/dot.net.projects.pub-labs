﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.DTO;
using DBAccessPerf.Helpers;
using DBAccessPerf.Entities;
using NHibernate.Criterion;
using NHibernate.Transform;


namespace DBAccessPerf.Tests
{
	public class TestTotalsOnOneTableNHibernateWithProjections : ITestRunnerWithTotals
	{
		public void Run(int customerID, out decimal countOfOrders, out decimal totalGrossAmount)
		{
			countOfOrders = 0;
			totalGrossAmount = 0;

			using (var session = new NHibernateSessionFactory().GetNewSession())
			{
				NHibernate.ICriteria qry = session.CreateCriteria<CustomerOrder>("co")
					.CreateAlias("Customer", "c")
					.Add(Expression.Eq("c.Id", customerID))
					.Add(Expression.Eq("co.IsTrash", false))
					.SetProjection(Projections.ProjectionList()
					// group by: .Add(Projections.Alias(Projections.GroupProperty("c.Id"), "customerId"))
						.Add(Projections.Alias(Projections.Count("co.Id"), "count"))
						.Add(Projections.Alias(Projections.Sum("co.PriceGross"), "total")));

				object[] data = qry.List<object[]>().FirstOrDefault();

				decimal.TryParse("" + data[0], out countOfOrders);
				decimal.TryParse("" + data[1], out totalGrossAmount);
			}
		}
	}
}
