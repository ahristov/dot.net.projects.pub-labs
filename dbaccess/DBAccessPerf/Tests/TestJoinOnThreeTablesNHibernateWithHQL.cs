﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.DTO;
using DBAccessPerf.Helpers;
using DBAccessPerf.Entities;
using NHibernate;
using NHibernate.Transform;


namespace DBAccessPerf.Tests
{
	public class TestJoinOnThreeTablesNHibernateWithHQL : ITestRunnerWithJoin
	{
		public IEnumerable<OrderSelection> Run(int customerID)
		{
			string hql = "";

			hql += "SELECT		c.Id as CustomerId,								\n";
			hql += "			c.Name as CustomerName,							\n";
			hql += "			co.Id as CustomerOrderId,						\n";
			hql += "			co.PriceGross as PriceGross,					\n";
			hql += "			st.Name as SubscriptionTypeName					\n";
			hql += "FROM		Customer as c									\n";
			hql += "				inner join c.Orders as co					\n";
			hql += "				inner join co.SubscriptionType as st		\n";
			hql += "WHERE 	c.Id = :customerID									\n";
			hql += "		and co.IsTrash = false								\n";


			using (var session = new NHibernateSessionFactory().GetNewSession())
			{
				IQuery qry = session.CreateQuery(hql)
					.SetInt32("customerID", customerID);

				qry.SetResultTransformer(Transformers.AliasToBean<OrderSelection>());
				return qry.List<OrderSelection>();
			}
		}
	}
}
