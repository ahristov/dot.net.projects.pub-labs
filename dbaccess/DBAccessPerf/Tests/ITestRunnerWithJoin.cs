﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.DTO;

namespace DBAccessPerf.Tests
{
	public interface ITestRunnerWithJoin
	{
		IEnumerable<OrderSelection> Run(int customerID);
	}
}
