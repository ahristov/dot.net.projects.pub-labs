﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.DTO;
using DBAccessPerf.Helpers;
using DBAccessPerf.Entities;


namespace DBAccessPerf.Tests
{
	public class TestSelectOnOneTableNHibernateWithCriteria : ITestRunnerWithSelect
	{
		public string Run(int customerID)
		{
			String customerName = "";

			using (var session = new NHibernateSessionFactory().GetNewSession())
			{
				Customer customer = session.Get<Customer>(customerID);

				if (customer != null)
				{
					customerName = customer.Name;
				}
			}

			return customerName;
		}
	}
}
