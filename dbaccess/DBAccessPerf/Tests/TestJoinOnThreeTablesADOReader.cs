﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.Helpers;
using System.Data.SqlClient;
using DBAccessPerf.DTO;


namespace DBAccessPerf.Tests
{
	public class TestJoinOnThreeTablesADOReader : ITestRunnerWithJoin
	{
		public IEnumerable<OrderSelection> Run(int customerID)
		{
			string sql = "";

			sql += "SELECT      c.customer_id, c.customer_name, co.customer_order_id, co.price_gross, st.subscription_type_name  \n";
			sql += "FROM        fpo_customers AS c INNER JOIN																	 \n";
			sql += "				fpo_customer_orders AS co ON co.customer_id = c.customer_id INNER JOIN						 \n";
			sql += "				fpo_subscription_types_lookup AS st ON st.subscription_type_id = co.subscription_type_id	 \n";
			sql += "WHERE       (co.is_trash = 0) AND (c.customer_id = "+customerID+")											 \n";
			sql += "ORDER BY	co.customer_order_id																			 \n";

			using (var cnn = ConnectionFactory.GetNewConnection())
			{
				var command = new SqlCommand(sql, cnn);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						yield return new OrderSelection
						{
							CustomerId = (Int32)reader["customer_id"],
							CustomerName = (String)reader["customer_name"],
							CustomerOrderId = (Int32)reader["customer_order_id"],
							PriceGross = (Decimal)reader["price_gross"],
							SubscriptionTypeName = (String)reader["subscription_type_name"],
						};
					}
				}
			}

		}
	}
}
