﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.Helpers;
using System.Data.SqlClient;
using DBAccessPerf.DTO;
using Dapper;

namespace DBAccessPerf.Tests
{
	public class TestTotalsOnOneTableDapper : ITestRunnerWithTotals
	{
		public void Run(int customerID, out decimal countOfOrders, out decimal totalGrossAmount)
		{
			countOfOrders = 0;
			totalGrossAmount = 0;
			string sql = "";

			sql += "SELECT		COUNT(*) AS count, SUM(price_gross) AS total		\n";
			sql += "FROM		fpo_customer_orders									\n";
			sql += "WHERE		(customer_id = "+customerID+")						\n";
			sql += "			AND (is_trash = 0)									\n";

			using (var cnn = ConnectionFactory.GetNewConnection())
			{
				decimal count = 0;
				decimal total = 0;

                cnn.Query<decimal, decimal, decimal>(sql,
                    (c, t) =>
                    {
						count = c;
						total = t;

						return 0;
                    }).First();

                countOfOrders = count;
                totalGrossAmount = total;

			}
		}
	}
}
