﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.DTO;
using DBAccessPerf.Helpers;
using DBAccessPerf.Entities;
using NHibernate;
using NHibernate.Transform;


namespace DBAccessPerf.Tests
{
	public class TestSelectOnOneTableNHibernateWithHQL : ITestRunnerWithSelect
	{

		public string Run(int customerID)
		{
			String customerName = "";
			string hql = "";

			hql += "SELECT		Name					\n";
			hql += "FROM		Customer				\n";
			hql += "WHERE		Id = :customerID		\n";

			using (var session = new NHibernateSessionFactory().GetNewSession())
			{
				IQuery qry = session.CreateQuery(hql)
					.SetInt32("customerID", customerID);

				customerName = qry.List<string>().FirstOrDefault();
			}

			return customerName;
		}
	}
}
