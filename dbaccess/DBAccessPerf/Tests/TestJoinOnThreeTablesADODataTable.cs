﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBAccessPerf.Helpers;
using System.Data.SqlClient;
using DBAccessPerf.DTO;
using System.Data;


namespace DBAccessPerf.Tests
{
	public class TestJoinOnThreeTablesADODataTable : ITestRunnerWithJoin
	{
		public IEnumerable<OrderSelection> Run(int customerID)
		{
			string sql = "";

			sql += "SELECT      c.customer_id, c.customer_name, co.customer_order_id, co.price_gross, st.subscription_type_name  \n";
			sql += "FROM        fpo_customers AS c INNER JOIN																	 \n";
			sql += "				fpo_customer_orders AS co ON co.customer_id = c.customer_id INNER JOIN						 \n";
			sql += "				fpo_subscription_types_lookup AS st ON st.subscription_type_id = co.subscription_type_id	 \n";
			sql += "WHERE       (co.is_trash = 0) AND (c.customer_id = "+customerID+")											 \n";
			sql += "ORDER BY	co.customer_order_id																			 \n";

			using (var cnn = ConnectionFactory.GetNewConnection())
			{
				var command = new SqlCommand(sql, cnn);
				var adapter = new SqlDataAdapter(command);
				var dataSet = new DataSet();

				adapter.Fill(dataSet, "OrderSelection");

				var dataTable = dataSet.Tables["OrderSelection"];


				foreach (DataRow dr in dataTable.Rows)
				{
					yield return new OrderSelection
					{
						CustomerId = (Int32)dr["customer_id"],
						CustomerName = (String)dr["customer_name"],
						CustomerOrderId = (Int32)dr["customer_order_id"],
						PriceGross = (Decimal)dr["price_gross"],
						SubscriptionTypeName = (String)dr["subscription_type_name"],
					};
				}
			}

		}
	}
}
