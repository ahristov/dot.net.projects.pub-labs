﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using DBAccessPerf.Tests;
using DBAccessPerf.Helpers;
using DBAccessPerf.DTO;

namespace DBAccessPerf
{
	// run "countIterations" times a test of some type
	public class TestRunner
	{
		private static Dictionary<Type, ITestRunnerWithJoin> _testRunnersWithJoin;
		private static Dictionary<Type, ITestRunnerWithTotals> _testRunnersWithTotals;
		private static Dictionary<Type, ITestRunnerWithSelect> _testRunnersWithSelect;
		private static DelimitedListTraceListener _listener;
		

		private int _countIterations;

		public TestRunner(String outputFileName, int countIterations)
		{

			if (_testRunnersWithJoin == null)
			{
				_testRunnersWithJoin = new Dictionary<Type, ITestRunnerWithJoin>()
				{
					{ typeof(TestJoinOnThreeTablesADOReader), new TestJoinOnThreeTablesADOReader() },
					{ typeof(TestJoinOnThreeTablesADODataTable), new TestJoinOnThreeTablesADODataTable() },
                    { typeof(TestJoinOnThreeTablesDapper), new TestJoinOnThreeTablesDapper() },
					{ typeof(TestJoinOnThreeTablesNHibernateWithCriteria), new TestJoinOnThreeTablesNHibernateWithCriteria() },
					{ typeof(TestJoinOnThreeTablesNHibernateWithProjections), new TestJoinOnThreeTablesNHibernateWithProjections() },
					{ typeof(TestJoinOnThreeTablesNHibernateWithHQL), new TestJoinOnThreeTablesNHibernateWithHQL() },
				};
			}

			if (_testRunnersWithTotals == null)
			{
				_testRunnersWithTotals= new Dictionary<Type, ITestRunnerWithTotals>()
				{
					{ typeof(TestTotalsOnOneTableADODataTable), new TestTotalsOnOneTableADODataTable() },
					{ typeof(TestTotalsOnOneTableADOReader), new TestTotalsOnOneTableADOReader() },
                    { typeof(TestTotalsOnOneTableDapper), new TestTotalsOnOneTableDapper() },
					{ typeof(TestTotalsOnOneTableNHibernateWithProjections), new TestTotalsOnOneTableNHibernateWithProjections() },
					{ typeof(TestTotalsOnOneTableNHibernateWithHQL), new TestTotalsOnOneTableNHibernateWithHQL() },
				};
			}

			if (_testRunnersWithSelect == null)
			{
				_testRunnersWithSelect = new Dictionary<Type, ITestRunnerWithSelect>()
				{
					{ typeof(TestSelectOnOneTableADOReader), new TestSelectOnOneTableADOReader() },
					{ typeof(TestSelectOnOneTableADODataTable), new TestSelectOnOneTableADODataTable() },
                    { typeof(TestSelectOnOneTableDapper), new TestSelectOnOneTableDapper() },
					{ typeof(TestSelectOnOneTableNHibernateWithCriteria), new TestSelectOnOneTableNHibernateWithCriteria() },
					{ typeof(TestSelectOnOneTableNHibernateWithHQL), new TestSelectOnOneTableNHibernateWithHQL() },
				};
			}

			if (_listener == null)
			{
				_listener = new DelimitedListTraceListener(outputFileName);
			}


			_countIterations = countIterations;

			Trace.Listeners.Clear();
			Trace.Listeners.Add(_listener);
			Trace.AutoFlush = true;
		}

		public void DoForTestRunnerWithJoin(String testName, Type type)
		{
			ITestRunnerWithJoin runner = _testRunnersWithJoin[type];

            runner.Run(1);

			using (var timer = new TimeIt(testName, type))
			{
				for (int i = 0; i < _countIterations; i++)
				{
                    Random random = new Random();
                    int customerID = random.Next(1, 10000);

                    foreach (OrderSelection orderSelection in runner.Run(customerID))
                    {
                        Console.WriteLine(String.Format("First rec is - CustomerId:{0,-6}, CustomerName:{1,-23}, CustomerOrderId:{2,-9}, PriceGross:{3,9}, SubscriptionTypeName:{4}",
                            orderSelection.CustomerId,
                            orderSelection.CustomerName,
                            orderSelection.CustomerOrderId,
                            orderSelection.PriceGross,
                            orderSelection.SubscriptionTypeName));
                        break;
                    }
				}

				//timer.Stop();
			}
		}


		public void DoForTestRunnerWithTotal(String testName, Type type)
		{
			ITestRunnerWithTotals runner = _testRunnersWithTotals[type];

			decimal countOfOrders = 0;
			decimal totalGrossAmount = 0;
            runner.Run(1, out countOfOrders, out totalGrossAmount);

			using (var timer = new TimeIt(testName, type))
			{
				for (int i = 0; i < _countIterations; i++)
				{
					Random random = new Random();
					int customerID = random.Next(1, 10000);


					runner.Run(customerID, out countOfOrders, out totalGrossAmount);

					Console.WriteLine("Customer: {0,-6} has {1,4} orders with total gross amount {2, 9}",
						customerID,
						countOfOrders,
						totalGrossAmount);
				}

				//timer.Stop();
			}
		}

		public void DoForTestRunnerWithSelect(String testName, Type type)
		{
			ITestRunnerWithSelect runner = _testRunnersWithSelect[type];
            
            runner.Run(1);

			using (var timer = new TimeIt(testName, type))
			{
				for (int i=0; i < _countIterations; i++)
				{
					Random random = new Random();
					int customerID = random.Next(1, 10000);

					string customerName = runner.Run(customerID);

					Console.WriteLine("Customer: {0,-6} has name \"{1}\"",
						customerID,
						customerName);
				}

				//timer.Stop();
			}
		}

	}
}
