﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;


/*
 * Option:
 * - migrate:  -task migrate
 * - rollback: -task rollback -steps 1
 * - profile:  -task migrate -profile Development
 * - preview:  -p
 * - verbose:  -verbose true
 * 
 * 
 * Example migrate to next:
 * 
 * C:\repo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf>packages\FluentMigrator.Tools.1.0.1.0\tools\x86\40\Migrate.exe  -conn "Data Source=.\SQLEXPRESS;AttachDbFilename=C:\repo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf\DbAccessPerf.mdf;Integrated Security=True;Connect Timepo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf\bin\Debug\DBAccessPerf.exe  -db SqlServer2008 -task migrate
 * 
 * Example rollback one step:
 * 
 * C:\repo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf>packages\FluentMigrator.Tools.1.0.1.0\tools\x86\40\Migrate.exe  -conn "Data Source=.\SQLEXPRESS;AttachDbFilename=C:\repo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf\DbAccessPerf.mdf;Integrated Security=True;Connect Timepo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf\bin\Debug\DBAccessPerf.exe  -db SqlServer2008 -task rollback -steps 1
 * 
 * Example run profile (can not rollback, there's no tracking):
 * 
 * C:\repo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf>packages\FluentMigrator.Tools.1.0.1.0\tools\x86\40\Migrate.exe  -conn "Data Source=.\SQLEXPRESS;AttachDbFilename=C:\repo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf\DbAccessPerf.mdf;Integrated Security=True;Connect Timepo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf\bin\Debug\DBAccessPerf.exe  -db SqlServer2008 -task migrate -profile InsertTestData
 * 
 * Example pust build task:
 * 
 * $(ProjectDir)packages\FluentMigrator.Tools.1.0.1.0\tools\x86\40\Migrate.exe  -conn "Data Source=.\SQLEXPRESS;AttachDbFilename=$(ProjectDir)DbAccessPerf.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True" -assembly $(ProjectDir)$(OutDir)DBAccessPerf.exe  -db SqlServer2008 -profile Production
 * 
 */

namespace DBAccessPerf.Migrations
{
	[Migration(1)]
	public class CreateTableCustomers : Migration
	{
		public override void Up()
		{
			Create.Table("fpo_customers")
				.WithColumn("customer_id").AsInt32().NotNullable().PrimaryKey().Identity()
				.WithColumn("customer_name").AsString(300).NotNullable().WithDefaultValue("")
				.WithColumn("is_trash").AsBoolean().NotNullable().WithDefaultValue(false);

		}

		public override void Down()
		{
			Delete.Table("fpo_customers");
		}
	}

	[Migration(2)]
	public class CreateTableOrders : Migration
	{
		public override void Up()
		{
			Create.Table("fpo_customer_orders")
				.WithColumn("customer_order_id").AsInt32().NotNullable().PrimaryKey().Identity()
				.WithColumn("is_trash").AsBoolean().NotNullable().WithDefaultValue(false)
				.WithColumn("customer_id").AsInt32().NotNullable()
				.WithColumn("subscription_type_id").AsInt32().NotNullable()
				.WithColumn("price_gross").AsDecimal().NotNullable().WithDefaultValue(0)
				.WithColumn("vat_percentage").AsDecimal().NotNullable().WithDefaultValue(0)
				.WithColumn("vat_amount").AsDecimal().NotNullable().WithDefaultValue(0)
				.WithColumn("price_net").AsDecimal().NotNullable().WithDefaultValue(0);
		}

		public override void Down()
		{
			Delete.Table("fpo_customer_orders");
		}
	}

	[Migration(3)]
	public class CreateTableSubscriptionType : Migration
	{
		public override void Up()
		{
			Create.Table("fpo_subscription_types_lookup")
				.WithColumn("subscription_type_id").AsInt32().NotNullable().PrimaryKey().Identity()
				.WithColumn("subscription_type_name").AsString(300).NotNullable().WithDefaultValue("")
				.WithColumn("subscription_type_period").AsString(25).NotNullable().WithDefaultValue("d")
				.WithColumn("subscription_type_length").AsDecimal().NotNullable().WithDefaultValue(0)
				.WithColumn("is_active").AsBoolean().NotNullable().WithDefaultValue(true)
				.WithColumn("current_price_gross").AsDecimal().NotNullable().WithDefaultValue(true)
				.WithColumn("online_from").AsDate().Nullable()
				.WithColumn("online_thru").AsDate().Nullable()
				.WithColumn("dorder").AsInt16().NotNullable().WithDefaultValue(0)
				.WithColumn("count_seats").AsInt16().NotNullable().WithDefaultValue(1);

		}

		public override void Down()
		{
			Delete.Table("fpo_subscription_types_lookup");
		}
	}


	[Migration(4)]
	public class InsertIntoSubscriptionType : Migration
	{
		public override void Up()
		{
			Insert.IntoTable("fpo_subscription_types_lookup")
				.Row(new
				{
					subscription_type_name = "Day Pass",
					subscription_type_period = "d",
					subscription_type_length = 1,
					is_active = false,
					current_price_gross = 5m,
					online_from = new DateTime(2011,8,1),
					online_thru = new DateTime(2011,9,1),
					dorder = 90,
					count_seats = 1,

				});


		}

		public override void Down()
		{
			Delete.FromTable("fpo_subscription_types_lookup").Row(new
			{
				subscription_type_name = "Day Pass",
				subscription_type_period = "d",
			});
		}
	}

	[Migration(5)]
	public class InsertIntoSubscriptionTypeMonthYearAnd14Days : Migration
	{
		public override void Up()
		{
			Insert.IntoTable("fpo_subscription_types_lookup")
				.Row(new
				{
					subscription_type_name = "14 Days Free Trial",
					subscription_type_period = "d",
					subscription_type_length = 14,
					is_active = true,
					current_price_gross = 0m,
					dorder = 99,
					count_seats = 1,
				});


			Insert.IntoTable("fpo_subscription_types_lookup")
				.Row(new
				{
					subscription_type_name = "One Month Membership",
					subscription_type_period = "m",
					subscription_type_length = 1,
					is_active = true,
					current_price_gross = 9m,
					online_from = new DateTime(2011, 8, 1),
					dorder = 85,
					count_seats = 1,
				});
			Insert.IntoTable("fpo_subscription_types_lookup")
				.Row(new
				{
					subscription_type_name = "One Year Membership",
					subscription_type_period = "y",
					subscription_type_length = 1,
					is_active = true,
					current_price_gross = 49m,
					online_from = new DateTime(2011, 8, 1),
					dorder = 80,
					count_seats = 1,
				});
			Insert.IntoTable("fpo_subscription_types_lookup")
				.Row(new
				{
					subscription_type_name = "One Year Membership and 5 Seats",
					subscription_type_period = "y",
					subscription_type_length = 1,
					is_active = true,
					current_price_gross = 225m,
					online_from = new DateTime(2011, 8, 1),
					dorder = 25,
					count_seats = 5,
				});
			Insert.IntoTable("fpo_subscription_types_lookup")
				.Row(new
				{
					subscription_type_name = "One Year Membership and 10 Seats",
					subscription_type_period = "y",
					subscription_type_length = 1,
					is_active = true,
					current_price_gross = 450m,
					online_from = new DateTime(2011, 8, 1),
					dorder = 20,
					count_seats = 10,
				});



		}

		public override void Down()
		{
			Delete.FromTable("fpo_subscription_types_lookup");
		}
	}


	[Migration(6)]
	public class CreateForeignKeys : Migration
	{
		public override void Up()
		{
			Create.ForeignKey("fpo_customer_orders_fk_fpo_customers")
				.FromTable("fpo_customer_orders").ForeignColumn("customer_id")
				.ToTable("fpo_customers").PrimaryColumn("customer_id");

			Create.ForeignKey("fpo_customer_orders_fk_fpo_subscription_types_lookup")
				.FromTable("fpo_customer_orders").ForeignColumn("subscription_type_id")
				.ToTable("fpo_subscription_types_lookup").PrimaryColumn("subscription_type_id");

		}

		public override void Down()
		{
			Delete.ForeignKey("fpo_customer_orders_fk_fpo_customers").OnTable("fpo_customer_orders");
			Delete.ForeignKey("fpo_customer_orders_fk_fpo_subscription_types_lookup").OnTable("fpo_customer_orders");
		}
	}




	[Profile("InsertTestData")]
	public class InsertTestCustomers : Migration
	{
		public override void Up()
		{
			for (int i = 1; i <= 10000; i++)
			{
				Insert.IntoTable("fpo_customers")
					.Row(new
					{
						customer_name = "Test Customer " + i,
					});

				for (int j = 1; j <= 100; j++)
				{
					Insert.IntoTable("fpo_customer_orders")
						.Row(new
						{
							customer_id = i,
							subscription_type_id = 3,
							price_gross = 100,
							vat_percentage = 10,
							vat_amount = 10,
							price_net = 90,
						});
				}

			}
		}

		public override void Down()
		{
		}

	}


}
