﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccessPerf.Entities
{
	public class CustomerOrder
	{
		public Int32 Id { get; set; }
		public Boolean IsTrash { get; set; }

		public Customer Customer { get; set; }
		public SubscriptionType SubscriptionType { get; set; }

		public Decimal PriceGross { get; set; }
		public Decimal VatPercentage { get; set; }
		public Decimal VatAmount { get; set; }
		public Decimal PriceNet { get; set; }
	}
}
