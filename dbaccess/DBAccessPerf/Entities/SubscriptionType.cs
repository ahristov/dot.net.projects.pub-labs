﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccessPerf.Entities
{
	public class SubscriptionType
	{
		public Int32 Id { get; set; }
		public String Name { get; set; }
		public String Period { get; set; }
		public Decimal Length { get; set; }
		public Boolean IsActive { get; set; }
		public Decimal CurrentPriceGross { get; set; }
		public DateTime? OnlineFrom { get; set; }
		public DateTime? OnlineThru { get; set; }
		public Int16 Dorder { get; set; }
		public Int16 CountSeats { get; set; }

		public IList<CustomerOrder> Orders { get; set; }
	}
}
