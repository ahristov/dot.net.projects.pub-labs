﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBAccessPerf.Entities
{
	public class Customer
	{
		public Int32 Id { get; set; }
		public String Name { get; set; }
		public Boolean IsTrash { get; set; }

        public IList<CustomerOrder> Orders { get; set; }
	}
}
