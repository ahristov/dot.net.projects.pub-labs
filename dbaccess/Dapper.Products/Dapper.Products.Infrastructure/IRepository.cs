﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products.Infrastructure
{
	public interface IRepository<TAggregateRoot>
		where TAggregateRoot : IAggregateRoot
	{
		//void Save(TAggregateRoot entity);
		//void Remove(TAggregateRoot entity);

		TAggregateRoot FindById(Int32 id);
	}
}
