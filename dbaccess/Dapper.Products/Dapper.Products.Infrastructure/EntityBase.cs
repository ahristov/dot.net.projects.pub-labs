﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products.Infrastructure
{
	public abstract class EntityBase<TEntity> : IEntity
		where TEntity : class, IEntity
	{
		private Int32 id;
		private Boolean isDurty;

		public Int32 Id
		{
			get { return id; }
			protected set { id = value; }
		}

		public Boolean IsTransient
		{
			get
			{
				return id == 0;
			}
		}

		public Boolean IsDurty
		{
			get { return isDurty; }
			protected set { isDurty = value; }
		}



		#region Equals

		public override Boolean Equals(Object obj)
		{
			TEntity entityToCompare = obj as TEntity;

			if (obj == null)
			{
				// is the passed object is null, they cannot be equal
				return false;
			}

			if (entityToCompare == null)
			{
				// if the passed object is not TEntity, they cannot be equal
				return false;
			}

			if (this.HasSameIdentifierAs(entityToCompare))
			{
				// Both entities are persistent so we are able
				// to compare the identifiers
				return true;
			}
			else if (this.HasDifferentPersistanceContextAs(entityToCompare))
			{
				// One entity is transient on is
				// persistens so that cannot be equal
				return false;
			}
			else
			{
				// Neither entity has an identity
				return false;
			}

		}

		public override Int32 GetHashCode()
		{
			if (this.IsTransient)
			{
				return this.GetType().FullName.GetHashCode();
			}
			else
			{
				return String.Format("{0}{1}",
					this.GetType().FullName, Id.ToString()).GetHashCode();
			}
		}

		private Boolean HasSameIdentifierAs(TEntity entityToCompare)
		{
			if (this.IsTransient || entityToCompare.IsTransient)
			{
				return false;
			}
			else
			{
				return this.Id == entityToCompare.Id;
			}
		}

		private Boolean HasDifferentPersistanceContextAs(TEntity entityToCompare)
		{
			return !(this.IsTransient == entityToCompare.IsTransient);
		}


		#endregion Equals

	}
}
