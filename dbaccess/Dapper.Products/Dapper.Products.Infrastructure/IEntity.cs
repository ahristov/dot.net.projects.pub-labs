﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products.Infrastructure
{
	public interface IEntity
	{
		Int32		Id { get; }
		Boolean		IsTransient { get; }
		Boolean		IsDurty { get; }

	}
}
