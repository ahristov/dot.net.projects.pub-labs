﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Dapper.Products.Infrastructure
{
	public interface IValidator
	{
		void Validate();
		ReadOnlyCollection<String> GetErrorMessages();
	}
}
