﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Products.Infrastructure;

namespace Dapper.Products.Model
{
	public class CouponType : EntityBase<CouponType>
	{
		public Int32		Coupon_type_id { get { return Id; } set { Id = value; } }

		public String		Coupon_type_name { get; set; }
		public Byte			Is_active { get; set; }
		public Decimal		Discount_amount { get; set; }
		public Decimal		Discount_percentage { get; set; }
		public Int32		Subscription_type_id { get; set; }	// TODO: assosiation
	}
}
