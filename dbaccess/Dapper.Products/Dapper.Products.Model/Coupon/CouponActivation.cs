﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Products.Infrastructure;

namespace Dapper.Products.Model
{
	public class CouponActivation : EntityBase<CouponActivation>
	{
		public Int32		Coupon_activation_id { get { return Id; } set { Id = value; } }

		public String		Coupon_activation_name { get; set; }
		public Byte			Is_trash { get; set; }
		public DateTime		Coupon_activation_trash_date { get; set; }
		public Byte			Is_archive { get; set; }
		public DateTime		Coupon_activation_archive_date { get; set; }
		public DateTime		Coupon_activation_update_date { get; set; }
		public DateTime		Coupon_activation_create_date { get; set; }
		public Int32		Activated_by_user_id { get; set; }
		public Int32		Activated_for_customer_id { get; set; }

		public Coupon		Coupon { get; set; }
	}
}
