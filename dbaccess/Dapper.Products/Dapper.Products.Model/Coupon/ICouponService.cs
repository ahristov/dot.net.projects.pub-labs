﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Dapper.Products.Model
{
	public interface ICouponService
	{
		Coupon				FindCouponById(Int32 couponId);
		Coupon				FindCouponByCode(String couponCode);

		CouponActivation	FindCouponActivationById(Int32 couponActivationId);
		CouponType			FindCouponTypeById(Int32 couponTypeId);

		CouponActivation	ActivateCoupon(
								Int32 userId, 
								Int32 customerId, 
								String couponCode, 
								out ReadOnlyCollection<String> errorMessages);


	}
}
