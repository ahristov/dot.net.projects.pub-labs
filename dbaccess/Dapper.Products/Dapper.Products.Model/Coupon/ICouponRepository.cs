﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Products.Infrastructure;

namespace Dapper.Products.Model
{
	public interface ICouponRepository : IRepository<Coupon>
	{
		Coupon				FindByCode(String code);

		CouponActivation	FindCouponActivationById(Int32 id);
		CouponType			FindCouponTypeById(Int32 id);

		CouponActivation	ActivateCoupon(Int32 couponId, Int32 activatedByUserId, Int32 activatedForCustomerId);
	}
}
