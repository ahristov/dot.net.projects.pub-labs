﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Products.Infrastructure;

namespace Dapper.Products.Model
{
	public class Coupon : EntityBase<Coupon>, IAggregateRoot
	{
		public Int32		Coupon_id { get { return Id; } set { Id = value; } }

		public String		Coupon_name { get; set; }
		public Byte			Is_trash { get; set; }
		public DateTime		Coupon_trash_date { get; set; }
		public Byte			Is_archive { get; set; }
		public DateTime		Coupon_archive_date { get; set; }
		public DateTime		Coupon_update_date { get; set; }
		public String		Coupon_code { get; set; }
		public String		Coupon_description { get; set; }
		public DateTime		Coupon_start_date { get; set; }
		public DateTime		Coupon_expiration_date { get; set; }
		public Int32		Coupon_usage_limit { get; set; }
		public Int32		Coupon_times_used { get; set; }
		public DateTime		Coupon_last_activation_date { get; set; }
		public Int32		Created_by_user_id { get; set; }			// assosiation
		public Int32		Issued_for_customer_id { get; set; }		// assosiation

		public CouponType	CouponType { get; set; }
	}
}
