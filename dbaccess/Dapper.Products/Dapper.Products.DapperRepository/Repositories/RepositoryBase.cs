﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Dapper;

namespace Dapper.Products.DapperRepository
{
	public abstract class RepositoryBase
	{
		protected IDbConnection dbConnection;

		public RepositoryBase(IDbConnection dbConnection)
		{
			this.dbConnection = dbConnection;
		}



		protected void GetLastId<T>(Action<T> setId)
		{
			dynamic identity = dbConnection.Query("SELECT @@IDENTITY AS Id").Single();
			T newId = (T)identity.Id;
			setId(newId);
		}

		protected Int32 GetLastId()
		{
			dynamic identity = dbConnection.Query("SELECT @@IDENTITY AS Id").Single();
			return (Int32)identity.Id;
		}

	}
}
