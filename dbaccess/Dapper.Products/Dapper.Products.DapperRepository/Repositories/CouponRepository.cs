﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Dapper.Products.Model;
using Products.Infrastructure;
using System.Data;

namespace Dapper.Products.DapperRepository
{
	public class CouponRepository : RepositoryBase, ICouponRepository
	{
		public CouponRepository(IDbConnection dbConnection)
			: base(dbConnection) { }

		public Coupon FindById(Int32 id)
		{
			String sql = "";

			sql += "select	c.*,      '' as split,															\n";
			sql += "		ctl.*,    '' as split															\n";
			sql += "from	prod_coupons c join																\n";
			sql += "		prod_coupon_types_lookup ctl on (ctl.coupon_type_id = c.coupon_type_id)			\n";
			sql += "where	c.coupon_id = "+id+"															\n";

			return dbConnection.Query<Coupon, CouponType, Coupon>(sql,
				(coupon, couponType) =>
				{
					coupon.CouponType = couponType;
					return coupon;
				},
				splitOn: "split").FirstOrDefault();
		}

		public Coupon FindByCode(String code)
		{
			String sql = "";

			sql += "select	c.*,      '' as split,															\n";
			sql += "		ctl.*,    '' as split															\n";
			sql += "from	prod_coupons c join																\n";
			sql += "		prod_coupon_types_lookup ctl on (ctl.coupon_type_id = c.coupon_type_id)			\n";
			sql += "where	c.coupon_code = '"+ code+"'														\n";

			return dbConnection.Query<Coupon, CouponType, Coupon>(sql,
				(coupon, couponType) =>
				{
					coupon.CouponType = couponType;
					return coupon;
				},
				splitOn: "split").FirstOrDefault();
		}

		public CouponActivation FindCouponActivationById(Int32 id)
		{
			String sql = "";

			sql += "select	ca.*,     '' as split,															\n";
			sql += "		c.*,      '' as split,															\n";
			sql += "		ctl.*,    '' as split															\n";
			sql += "from	prod_coupon_activations ca join													\n";
			sql += "		prod_coupons c on (c.coupon_id = ca.coupon_id) join								\n";
			sql += "		prod_coupon_types_lookup ctl on (ctl.coupon_type_id = c.coupon_type_id)			\n";
			sql += "where	ca.coupon_activation_id = "+id+"												\n";

			return dbConnection.Query<CouponActivation, Coupon, CouponType, CouponActivation>(sql,
				(couponActivation, coupon, couponType) =>
				{
					coupon.CouponType = couponType;
					couponActivation.Coupon = coupon;
					return couponActivation;
				},
				splitOn: "split").FirstOrDefault();

		}

		public CouponType FindCouponTypeById(Int32 id)
		{
			String sql = "";

			sql += "select	ctl.*																			\n";
			sql += "from	prod_coupon_types_lookup	ctl													\n";
			sql += "where	ctl.coupon_type_id = "+id+"														\n";

			return dbConnection.Query<CouponType>(sql).FirstOrDefault();
		}


		public CouponActivation ActivateCoupon(Int32 couponId, Int32 activatedByUserId, Int32 activatedForCustomerId)
		{
			String sql = "";
			Int32 newRecId = 0;

			sql += "insert into prod_coupon_activations (													\n";
			sql += "	coupon_id, activated_by_user_id, activated_for_customer_id							\n";
			sql += ") values (																				\n";
			sql += "	"+couponId+","+activatedByUserId+","+activatedForCustomerId+"						\n";
			sql += ")																						\n";

			dbConnection.Execute(sql);
			newRecId = GetLastId();

			if (newRecId > 0)
			{
				UpdateCouponTimesUsed(couponId);
				return FindCouponActivationById(newRecId);
			}
			else
			{
				return null;
			}
		}



		private void UpdateCouponTimesUsed(Int32 couponId)
		{
			String sql = "";

			sql += "update	prod_coupons																	\n";
			sql += "set		coupon_times_used = (															\n";
			sql += "			select	count(*)															\n";
			sql += "			from	prod_coupon_activations												\n";
			sql += "			where	coupon_id = "+couponId+"											\n";
			sql += "		)																				\n";
			sql += "where	coupon_id = "+couponId+"														\n";

			dbConnection.Execute(sql);
		}

	}
}
