﻿This project contains the migrations for creating the tables for the project.

The database in the usage examples below should exists already.


When running the FluentMigrations Migrate.exe tool the following options are important/useful:

FluentMigrate Option:
- migrate:  -task migrate							// runs upgrade to the lates version
- rollback: -task rollback -steps 1					// runs rollback to version "1"
- profile:  -task migrate -profile Development		// runs a named migration, a named migration is not part of the versioning
- preview:  -p										// runs just a preview and does not do anything to the database, use only in combination with other optons
- verbose:  -verbose true							// prints extra information as it runs


** Usage examples

*** Usage examples: Migrate to latest version

packages\FluentMigrator.Tools.1.0.1.0\tools\x86\40\Migrate.exe  -conn "Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Projects\dot.net.projects.pub-labs\dbaccess\Dapper.Products\Products.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True" C:\Projects\dot.net.projects.pub-labs\dbaccess\Dapper.Products\Dapper.Products.Migrations\bin\Debug\Dapper.Products.Migrations.dll  -db SqlServer2008 -task migrate

*** Usage examples: Rollback one step

C:\repo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf>packages\FluentMigrator.Tools.1.0.1.0\tools\x86\40\Migrate.exe  -conn "Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Projects\dot.net.projects.pub-labs\dbaccess\Dapper.Products\Products.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True" C:\Projects\dot.net.projects.pub-labs\dbaccess\Dapper.Products\Dapper.Products.Migrations\bin\Debug\Dapper.Products.Migrations.dll  -db SqlServer2008 -task rollback -steps 1

*** Usage examples: Run profile (can not rollback, there's no tracking)

C:\repo\dot.net.projects.pub-labs\dbaccess\DBAccessPerf>packages\FluentMigrator.Tools.1.0.1.0\tools\x86\40\Migrate.exe  -conn "Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Projects\dot.net.projects.pub-labs\dbaccess\Dapper.Products\Products.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True" C:\Projects\dot.net.projects.pub-labs\dbaccess\Dapper.Products\Dapper.Products.Migrations\bin\Debug\Dapper.Products.Migrations.dll  -db SqlServer2008 -task migrate -profile InsertTestData


*** Usage examples: Post Build Task

$(ProjectDir)packages\FluentMigrator.Tools.1.0.1.0\tools\x86\40\Migrate.exe  -conn "Data Source=.\SQLEXPRESS;AttachDbFilename=$(ProjectDir)Products.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True" -assembly $(ProjectDir)$(OutDir)Dapper.Products.Migrations.dll  -db SqlServer2008 -profile Production



