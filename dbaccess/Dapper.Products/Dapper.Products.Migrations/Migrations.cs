﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;

namespace Dapper.Products.Migrations
{
	public class Migrations
	{
		[Migration(1)]
		public class CreateTableCouponType : Migration
		{
			public override void Up()
			{
				Create.Table("prod_coupon_types_lookup")
					.WithColumn("Coupon_type_id").AsInt32().NotNullable().PrimaryKey().Identity()
					.WithColumn("Coupon_type_name").AsString(300).NotNullable().WithDefaultValue("")
					.WithColumn("Is_active").AsBoolean().NotNullable().WithDefaultValue(false)
					.WithColumn("Discount_amount").AsDecimal(19, 5).NotNullable().WithDefaultValue(0)
					.WithColumn("Discount_percentage").AsDecimal(19, 5).NotNullable().WithDefaultValue(0)
					.WithColumn("Subscription_type_id").AsInt32().NotNullable().WithDefaultValue(0);
			}

			public override void Down()
			{
				Delete.Table("prod_coupon_types");
			}
		}


		[Migration(2)]
		public class CreateTableCoupon : Migration
		{
			public override void Up()
			{
				Create.Table("prod_coupons")
					.WithColumn("Coupon_id").AsInt32().NotNullable().PrimaryKey().Identity()
					.WithColumn("Coupon_name").AsString(300).NotNullable().WithDefaultValue("")
					.WithColumn("Is_trash").AsBoolean().NotNullable().WithDefaultValue(false)
					.WithColumn("Coupon_trash_date").AsDateTime().Nullable()
					.WithColumn("Is_archive").AsBoolean().NotNullable().WithDefaultValue(false)
					.WithColumn("Coupon_archive_date").AsDateTime().Nullable()
					.WithColumn("Coupon_update_date").AsDateTime().Nullable()
					.WithColumn("Coupon_code").AsString(50).NotNullable().WithDefaultValue("")
					.WithColumn("Coupon_description").AsString(500).NotNullable().WithDefaultValue("")
					.WithColumn("Coupon_start_date").AsDateTime().Nullable()
					.WithColumn("Coupon_expiration_date").AsDateTime().Nullable()
					.WithColumn("Coupon_usage_limit").AsInt32().NotNullable().WithDefaultValue(0)
					.WithColumn("Coupon_times_used").AsInt32().NotNullable().WithDefaultValue(0)
					.WithColumn("Coupon_last_activation_date").AsDateTime().Nullable()
					.WithColumn("Created_by_user_id").AsInt32().NotNullable().WithDefaultValue(0)
					.WithColumn("Issued_for_customer_id").AsInt32().NotNullable().WithDefaultValue(0)
					.WithColumn("Coupon_type_id").AsInt32().NotNullable().WithDefaultValue(0);

			}

			public override void Down()
			{
				Delete.Table("prod_coupons");
			}
		}


		[Migration(3)]
		public class CreateTableCouponActivation : Migration
		{
			public override void Up()
			{
				Create.Table("prod_coupon_activations")
					.WithColumn("Coupon_activation_id").AsInt32().NotNullable().PrimaryKey().Identity()
					.WithColumn("Coupon_activation_name").AsString(300).NotNullable().WithDefaultValue("")
					.WithColumn("Is_trash").AsBoolean().NotNullable().WithDefaultValue(false)
					.WithColumn("Coupon_activation_trash_date").AsDateTime().Nullable()
					.WithColumn("Is_archive").AsBoolean().NotNullable().WithDefaultValue(false)
					.WithColumn("Coupon_activation_archive_date").AsDateTime().Nullable()
					.WithColumn("Coupon_activation_update_date").AsDateTime().Nullable()
					.WithColumn("Coupon_activation_create_date").AsDateTime().Nullable()
					.WithColumn("Activated_by_user_id").AsInt32().NotNullable().WithDefaultValue(0)
					.WithColumn("Activated_for_customer_id").AsInt32().NotNullable().WithDefaultValue(0)
					.WithColumn("Coupon_id").AsInt32().NotNullable().WithDefaultValue(0);
			}

			public override void Down()
			{
				Delete.Table("prod_coupon_activations");
			}
		}
	
	}
}
