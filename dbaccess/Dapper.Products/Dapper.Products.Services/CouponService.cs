﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper.Products.Model;
using System.Collections.ObjectModel;

namespace Dapper.Products.Services
{
	public class CouponService : ICouponService
	{
		ICouponRepository couponRepository;

		public CouponService(ICouponRepository couponRepository)
		{
			this.couponRepository = couponRepository;
		}



		public Coupon FindCouponById(Int32 couponId)
		{
			return couponRepository.FindById(couponId);
		}

		public Coupon FindCouponByCode(String couponCode)
		{
			return couponRepository.FindByCode(couponCode);
		}

		public CouponActivation FindCouponActivationById(Int32 couponActivationId)
		{
			return couponRepository.FindCouponActivationById(couponActivationId);
		}

		public CouponType FindCouponTypeById(Int32 couponTypeId)
		{
			return couponRepository.FindCouponTypeById(couponTypeId);
		}



		public CouponActivation ActivateCoupon(
			Int32 activatedByUserId, 
			Int32 activatedForCustomerId, 
			String couponCode, 
			out ReadOnlyCollection<String> errorMessages)
		{
			Coupon coupon = FindCouponByCode(couponCode);

			return ActivateCoupon(activatedByUserId, activatedForCustomerId, coupon, out errorMessages);
		}

		private CouponActivation ActivateCoupon(
			Int32 activatedByUserId, 
			Int32 activatedForCustomerId, 
			Coupon coupon, 
			out ReadOnlyCollection<String> errorMessages)
		{
			ValidateOnCouponActivation validator = new ValidateOnCouponActivation(activatedByUserId, activatedForCustomerId, coupon);

			validator.Validate();
			errorMessages = validator.GetErrorMessages();

			if (errorMessages.Count > 0)
			{
				return null;
			}
			else
			{
				return couponRepository.ActivateCoupon(coupon.Coupon_id, activatedByUserId, activatedForCustomerId);
			}
		}

	}


	
}
