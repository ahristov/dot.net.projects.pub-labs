﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper.Products.Model;
using Dapper.Products.Infrastructure;
using System.Collections.ObjectModel;

namespace Dapper.Products.Services
{
	public class ValidateOnCouponActivation : IValidator
	{
		private Int32 activatedByUserId;
		private Int32 activatedForCustomerId;
		private Coupon coupon;
		private List<String> errorMessages = new List<string>();

		public ValidateOnCouponActivation(Int32 activatedByUserId, Int32 activatedForCustomerId, Coupon coupon)
		{
			this.activatedByUserId = activatedByUserId;
			this.activatedForCustomerId = activatedForCustomerId;
			this.coupon = coupon;
		}



		public void Validate()
		{
			String errorMessage;

			errorMessages.Clear();


			errorMessage = CheckIfIssuedForAnotherCustomer();
			if (!String.IsNullOrEmpty(errorMessage))
			{
				errorMessages.Add(errorMessage);
			}

			errorMessage = CheckIfExpired();
			if (!String.IsNullOrEmpty(errorMessage))
			{
				errorMessages.Add(errorMessage);
			}


		}

		public ReadOnlyCollection<String> GetErrorMessages()
		{
			return new ReadOnlyCollection<string>(errorMessages);
		}


		#region Validation Helpers

		protected String CheckIfIssuedForAnotherCustomer()
		{
			if (coupon.Issued_for_customer_id == 0)
			{
				return String.Empty;
			}

			if (coupon.Issued_for_customer_id == activatedForCustomerId)
			{
				return String.Empty;
			}

			return "The coupon not issued for this customer.";
		}

		protected String CheckIfExpired()
		{
			if (coupon.Coupon_expiration_date == DateTime.MinValue)
			{
				return String.Empty;
			}

			if (coupon.Coupon_expiration_date >= DateTime.Now)
			{
				return String.Empty;
			}

			return "This coupon already expired.";
		}


		#endregion

	}
}
