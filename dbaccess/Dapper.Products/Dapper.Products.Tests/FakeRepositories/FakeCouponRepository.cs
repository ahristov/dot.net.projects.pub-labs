﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper.Products.Model;


// NOT USED SO FAR!!!

// Provide fake implementation of in-memory database

namespace Dapper.Products.Tests
{
	public class FakeCouponRepository : ICouponRepository
	{
		private List<CouponType> couponTypes;
		private List<Coupon> coupons;
		private List<CouponActivation> couponActivations;

		public FakeCouponRepository()
		{
			couponTypes = new List<CouponType>()
 			{
				new CouponType()
				{
					Coupon_type_id = 1,
					Coupon_type_name = "14 Days Free Trial",
					Is_active = 1,
					Discount_amount = 0m,
					Discount_percentage = 100m,
					Subscription_type_id = 16,
				},
			};

			coupons = new List<Coupon>()
			{
				new Coupon() 
				{
					Coupon_id = 1,
					CouponType = couponTypes.Where(ct => ct.Coupon_type_id == 1).FirstOrDefault(),
					Coupon_code = "14DaysFree-UsagesLimitOf2",
					Coupon_description = "14 Days Free Trial",
					Coupon_usage_limit = 2,
				},
				new Coupon() 
				{
					Coupon_id = 2,
					CouponType = couponTypes.Where(ct => ct.Coupon_type_id == 1).FirstOrDefault(),
					Coupon_code = "14DaysFree-IssuedForCustomer2",
					Coupon_description = "14 Days Free Trial",
					Coupon_usage_limit = 1,
					Issued_for_customer_id = 2,
				},
			};

			couponActivations = new List<CouponActivation>();
		}


		public Coupon FindById(int id)
		{
			return coupons.Where(c => c.Coupon_id == id).FirstOrDefault();
		}

		public Coupon FindByCode(string code)
		{
			return coupons.Where(c => c.Coupon_code == code).FirstOrDefault();
		}

		public CouponActivation FindCouponActivationById(int id)
		{
			throw new NotImplementedException();
		}

		public CouponType FindCouponTypeById(int id)
		{
			throw new NotImplementedException();
		}

		public CouponActivation ActivateCoupon(int couponId, int activatedByUserId, int activatedForCustomerId)
		{
			throw new NotImplementedException();
		}

	}
}
