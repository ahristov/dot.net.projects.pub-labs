﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Dapper.Products.Services;
using Dapper.Products.Model;

// Using a stub to change the default behaviour of the SUT.
// In this case it just will provide access to otherwise 
// not accessible functions.

namespace Dapper.Products.Tests.ServicesTests
{
	[TestFixture]
	public class ValidateOnCouponActivationTests : TestFixtureBase
	{

		private class ValidateOnCouponActivationSUT : ValidateOnCouponActivation
		{
			public ValidateOnCouponActivationSUT(Int32 activatedByUserId, Int32 activatedForCustomerId, Coupon coupon)
				: base(activatedByUserId, activatedForCustomerId, coupon) {}

			public new String CheckIfIssuedForAnotherCustomer()
			{
				return base.CheckIfIssuedForAnotherCustomer();
			}

			public new String CheckIfExpired()
			{
				return base.CheckIfExpired();
			}

		}




		[Test]
		public void CheckIfIssuedForAnotherCustomer_WhenNotIssuedForSpecificCustomer_ShouldNotReturnErrorMessage()
		{
			Coupon coupon = new Coupon()
			{
				Issued_for_customer_id = 0,
			};

			Int32 activatedByUserId = 1;
			Int32 activatedForCustomerId = 2;


			var sut = new ValidateOnCouponActivationSUT(activatedByUserId, activatedForCustomerId, coupon);

			String errorMessage = sut.CheckIfIssuedForAnotherCustomer();

			errorMessage.ShouldEqual(String.Empty);
			
		}


		[Test]
		public void CheckIfIssuedForAnotherCustomer_WhenIssuedForSameCustomer_ShouldNotReturnErrorMessage()
		{
			Coupon coupon = new Coupon()
			{
				Issued_for_customer_id = 2,
			};

			Int32 activatedByUserId = 1;
			Int32 activatedForCustomerId = 2;


			var sut = new ValidateOnCouponActivationSUT(activatedByUserId, activatedForCustomerId, coupon);

			String errorMessage = sut.CheckIfIssuedForAnotherCustomer();

			errorMessage.ShouldEqual(String.Empty);
		}



		[Test]
		public void CheckIfIssuedForAnotherCustomer_WhenIssuedForAnotherCustomer_ShouldReturnErrorMessage()
		{
			Coupon coupon = new Coupon()
			{
				Issued_for_customer_id = 999,
			};

			Int32 activatedByUserId = 1;
			Int32 activatedForCustomerId = 2;


			var sut = new ValidateOnCouponActivationSUT(activatedByUserId, activatedForCustomerId, coupon);

			String errorMessage = sut.CheckIfIssuedForAnotherCustomer();

			errorMessage.ShouldNotEqual(String.Empty);
		}



	}
}
