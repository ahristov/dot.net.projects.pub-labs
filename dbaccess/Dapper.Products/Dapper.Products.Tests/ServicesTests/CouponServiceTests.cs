﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Dapper.Products.Model;
using Moq;
using Dapper.Products.Services;
using System.Collections.ObjectModel;


// Using a mock to change the default behaviour of a service the SUT depends on.

namespace Dapper.Products.Tests.ServicesTests
{
	[TestFixture]
	public class CouponServiceTests : TestFixtureBase
	{
		#region Test it calls ValidateOnCouponActivation

		[Test]
		public void ActivateCoupon_WhenNotIssuedForSpecificCustomer_ShouldNotReturnErrorMessages()
		{
			// arrange
			String couponCode = "ABC123";
			Int32 activatedByUserId = 1;
			Int32 activatedForCustomerId = 1;
			Coupon coupon = new Coupon
			{
				Coupon_id = 1,
				Issued_for_customer_id = 0, // the coupon not issued to specific user 
			};

			var couponRepositoryMock = new Mock<ICouponRepository>();
			couponRepositoryMock.Setup(o => o.FindByCode(couponCode)).Returns(coupon);

			ICouponRepository couponRepository = couponRepositoryMock.Object;
			CouponService couponService = new CouponService(couponRepository);
			ReadOnlyCollection<String> errorMessages;

			// act
			couponService.ActivateCoupon(activatedByUserId, activatedForCustomerId, couponCode, out errorMessages);


			// assert
			errorMessages.Count.ShouldEqual(0);
		}

		[Test]
		public void ActivateCoupon_WhenIssuedForSameCustomer_ShouldNotReturnErrorMessages()
		{
			// arrange
			String couponCode = "ABC123";
			Int32 activatedByUserId = 1;
			Int32 activatedForCustomerId = 1; // this is same
			Coupon coupon = new Coupon
			{
				Coupon_id = 1,
				Issued_for_customer_id = 1, // as this 
			};

			var couponRepositoryMock = new Mock<ICouponRepository>();
			couponRepositoryMock.Setup(o => o.FindByCode(couponCode)).Returns(coupon);

			ICouponRepository couponRepository = couponRepositoryMock.Object;
			CouponService couponService = new CouponService(couponRepository);
			ReadOnlyCollection<String> errorMessages;

			// act
			couponService.ActivateCoupon(activatedByUserId, activatedForCustomerId, couponCode, out errorMessages);


			// assert
			errorMessages.Count.ShouldEqual(0);
		}

		[Test]
		public void ActivateCoupon_WhenIssuedForAnotherCustomer_ShouldReturnErrorMessages()
		{
			// arrange
			String couponCode = "ABC123";
			Int32 activatedByUserId = 1;
			Int32 activatedForCustomerId = 1; // this is different
			Coupon coupon = new Coupon
			{
				Coupon_id = 1,
				Issued_for_customer_id = 999, // than this
			};

			var couponRepositoryMock = new Mock<ICouponRepository>();
			couponRepositoryMock.Setup(o => o.FindByCode(couponCode)).Returns(coupon);

			ICouponRepository couponRepository = couponRepositoryMock.Object;
			CouponService couponService = new CouponService(couponRepository);
			ReadOnlyCollection<String> errorMessages;


			// act
			couponService.ActivateCoupon(activatedByUserId, activatedForCustomerId, couponCode, out errorMessages);

			
			// assert
			errorMessages.Count.ShouldNotEqual(0);
		}

		#endregion Test it calls ValidateOnCouponActivation



		[Test]
		public void ActivateCoupon_WhenTheValidationWasSuccessful_ShouldCallTheRepositoryForSaving()
		{
			// arrange
			String couponCode = "ABC123";
			Int32 activatedByUserId = 1;
			Int32 activatedForCustomerId = 1;
			Coupon coupon = new Coupon
			{
				Coupon_id = 1,
				Issued_for_customer_id = 0,
			};


			// This is a flag which will show us
			// whether couponService.ActivateCoupon() 
			// has called the couponRepository.ActivateCoupon() 
			Boolean hasCalledActivateCoupon = false;

			var couponRepositoryMock = new Mock<ICouponRepository>();
			couponRepositoryMock.Setup(o => o.FindByCode(couponCode)).Returns(coupon);
			couponRepositoryMock.Setup(o => o.ActivateCoupon(
				coupon.Id,
				activatedByUserId,
				activatedForCustomerId)).Returns(() =>
					{
						// Voila! The couponService.ActivateCoupon()
						// calls couponRepository.ActivateCoupon() to persist the changes
						hasCalledActivateCoupon = true; 

						return null; // just whatever, we are not interested in the result right now.
					}
				);

			ICouponRepository couponRepository = couponRepositoryMock.Object;
			CouponService couponService = new CouponService(couponRepository);
			ReadOnlyCollection<String> errorMessages;

			// act
			couponService.ActivateCoupon(activatedByUserId, activatedForCustomerId, couponCode, out errorMessages);


			// assert
			hasCalledActivateCoupon.ShouldEqual(true);

		}



	}
}
