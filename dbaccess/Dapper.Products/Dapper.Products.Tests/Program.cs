﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper.Products.Model;
using Dapper.Products.DapperRepository;

namespace Dapper.Products.Tests
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var dbConnection = ConnectionFactory.GetNewConnection())
			{

				ICouponRepository couponRepository = new CouponRepository(dbConnection);

				CouponType couponType;
				Coupon coupon;
				CouponActivation couponActivation;

				couponType = couponRepository.FindCouponTypeById(5);
				Console.WriteLine(couponType.Coupon_type_id);

				coupon = couponRepository.FindById(20238);
				Console.WriteLine(coupon.Id);

				coupon = couponRepository.FindByCode("IWM01P00-201110");
				Console.WriteLine(coupon.Id);

				couponActivation = couponRepository.FindCouponActivationById(209);
				Console.WriteLine(couponActivation.Coupon.Id);
				Console.WriteLine(couponActivation.Coupon.CouponType.Coupon_type_name);


				dbConnection.Close();
				Console.WriteLine("Press <Enter> to finish.");
				Console.ReadLine();
			}

		}
	}
}
