﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Dapper.Products.Tests
{
	public static class ExtentionMethods
	{
		public static void ShouldEqual<T>(this T actual, T expected)
		{
			Assert.AreEqual(expected, actual);
		}

		public static void ShouldNotEqual<T>(this T actual, T expected)
		{
			Assert.AreNotEqual(expected, actual);
		}


		public static void Times(this int times, Action<int> action)
		{
			for (int i = 0; i < times; i++)
				action(i);
		}

		public static void Moan(this string s)
		{
			if (String.IsNullOrEmpty(s))
				return;

			Console.WriteLine(s);
		}

		public static List<T> AddRangeAndReturn<T>(this List<T> list, IEnumerable<T> range)
		{
			list.AddRange(range);
			return list;
		}
	}
}
