﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Data;

namespace Dapper.Products.Tests
{
	public class TestFixtureBase : IDisposable
	{
		protected Boolean isInitialized;
		protected IDbConnection dbConnection;

		[TestFixtureSetUp]
		protected void Init()
		{
			if (! isInitialized)
			{
				dbConnection = ConnectionFactory.GetNewConnection();
				isInitialized = true;
			}
			Assert.NotNull(dbConnection);
		}

		[TestFixtureTearDown]
		public void Dispose()
		{
			Console.WriteLine("Disposing...");
			try
			{
				if (dbConnection != null)
				{
					if (dbConnection.State == ConnectionState.Open)
					{
						dbConnection.Close();
					}
					dbConnection.Dispose();
				}
			}
			finally
			{
				dbConnection = null;
			}
		}

	}
}
