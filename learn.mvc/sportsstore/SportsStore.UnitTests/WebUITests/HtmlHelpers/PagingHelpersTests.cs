﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using A.TestFramework;
using System.Web.Mvc;
using SportsStore.WebUI.Models;
using SportsStore.WebUI.HtmlHelpers;
using A.TestFramework.Extensions;

namespace SportsStore.UnitTests.WebUITests.HtmlHelpers
{
    [TestFixture]
    public class PagingHelpersTests : BaseTestFixture<PagingHelpersTests, object>
    {
        [Test]
        public void Can_Generate_Page_Links()
        {
            // Arrange
            var sut = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10,
            };

            Func<int, string> pageUrlDelegate = i => "Page" + i;

            HtmlHelper htmlHelper = null; // in order to be able to use the extension methods

            // Act
            MvcHtmlString result = htmlHelper.PageLinks(sut, pageUrlDelegate);

            // Assert
            result.ToString().AssertEqual(@"<a href=""Page1"">1</a><a class=""selected"" href=""Page2"">2</a><a href=""Page3"">3</a>");

        }

    }
}
