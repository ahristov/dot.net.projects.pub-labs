﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using A.TestFramework;
using SportsStore.Domain.Abstract;
using Moq;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Controllers;
using A.TestFramework.Extensions;
using SportsStore.WebUI.Models;
using System.Web.Mvc;

namespace SportsStore.UnitTests.WebUITests.Controllers
{
    [TestFixture]
    public class ProductControllerTests : BaseTestFixture<ProductControllerTests, object>
    {

        [Test]
        public void Can_Paginate()
        {
            // Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product {Id=1, Name="P1"},
                new Product {Id=2, Name="P2"},
                new Product {Id=3, Name="P3"},
                new Product {Id=4, Name="P4"},
                new Product {Id=5, Name="P5"},
            }.AsQueryable());


            // Act
            var sut = new ProductController(mock.Object);
            sut.PageSize = 3;

            ProductsListViewModel result = (ProductsListViewModel)sut.List(2).ViewData.Model;

            // Assert
            Product[] arr = result.Products.ToArray();
            arr.Length.AssertEqual(2);
            arr[0].Name.AssertEqual("P4");
            arr[1].Name.AssertEqual("P5");
        }

        [Test]
        public void Can_Send_Pagination_View_Model()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product {Id=1, Name="P1"},
                new Product {Id=2, Name="P2"},
                new Product {Id=3, Name="P3"},
                new Product {Id=4, Name="P4"},
                new Product {Id=5, Name="P5"},
            }.AsQueryable());

            ProductController controller = new ProductController(mock.Object);
            controller.PageSize = 3;

            ProductsListViewModel result = (ProductsListViewModel)controller.List(2).ViewData.Model;

            PagingInfo pagingInfo = result.PagingInfo;

            pagingInfo.CurrentPage.AssertEqual(2);
            pagingInfo.ItemsPerPage.AssertEqual(3);
            pagingInfo.TotalItems.AssertEqual(5);
            pagingInfo.TotalPages.AssertEqual(2);
        }

    }
}
